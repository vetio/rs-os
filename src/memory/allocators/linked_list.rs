use crate::memory::allocators::{align_up, Allocator, AllocatorAddFreeRegion};
use crate::PAGE_ALLOCATOR;
use core::alloc::Layout;
use core::ptr::null_mut;

struct ListNode {
    size: usize,
    next: Option<&'static mut ListNode>,
}

impl ListNode {
    const fn new(size: usize) -> Self {
        ListNode { size, next: None }
    }

    fn start_addr(&self) -> usize {
        self as *const Self as usize
    }

    fn end_addr(&self) -> usize {
        self.start_addr() + self.size
    }
}

pub struct LinkedListAllocator<Fallback> {
    head: ListNode,
    fallback: Fallback,
}

impl<Fallback> LinkedListAllocator<Fallback> {
    pub const fn with_fallback(fallback: Fallback) -> Self {
        LinkedListAllocator {
            head: ListNode::new(0),
            fallback,
        }
    }
}

impl<Fallback: Allocator> LinkedListAllocator<Fallback> {
    pub unsafe fn init(&mut self, heap_start: usize, heap_size: usize) {
        self.add_free_region(heap_start as *mut u8, heap_size);
    }

    fn find_region(&mut self, size: usize, align: usize) -> Option<(&'static mut ListNode, usize)> {
        let mut current = &mut self.head;

        while let Some(ref mut region) = current.next {
            if let Ok(alloc_start) = Self::alloc_from_region(&region, size, align) {
                let next = region.next.take();
                let ret = (current.next.take().unwrap(), alloc_start);
                current.next = next;
                return Some(ret);
            } else {
                current = current.next.as_mut().unwrap();
            }
        }

        None
    }

    fn alloc_from_region(region: &ListNode, size: usize, align: usize) -> Result<usize, ()> {
        let alloc_start = align_up(region.start_addr(), align);
        let alloc_end = alloc_start.checked_add(size).ok_or(())?;

        if alloc_end > region.end_addr() {
            return Err(());
        }

        let excess_size = region.end_addr() - alloc_end;
        if excess_size > 0 && excess_size < core::mem::size_of::<ListNode>() {
            return Err(());
        }

        Ok(alloc_start)
    }

    fn size_align(layout: Layout) -> (usize, usize) {
        let layout = layout
            .align_to(core::mem::align_of::<ListNode>())
            .expect("adjusting alignment faied")
            .pad_to_align();
        let size = layout.size().max(core::mem::size_of::<ListNode>());
        (size, layout.align())
    }
}

unsafe impl<Fallback: Allocator> Allocator for LinkedListAllocator<Fallback> {
    fn alloc(&mut self, layout: Layout) -> *mut u8 {
        let (size, align) = Self::size_align(layout);

        let mut found = self.find_region(size, align);
        if found.is_none() {
            let num_pages = (layout.size() as u64 / 4096) + 1;
            if let Some(page_alloc) = PAGE_ALLOCATOR.get() {
                let page_range = match page_alloc
                    .lock()
                    .alloc_page_range_4kb(num_pages)
                {
                    Ok(page) => page,
                    Err(_) => return self.fallback.alloc(layout),
                };
                unsafe {
                    self.add_free_region(
                        page_range.start.start_address().as_mut_ptr(),
                        (page_range.start.size() * (page_range.end - page_range.start + 1)) as usize,
                    )
                };
                found = self.find_region(size, align);
            }
        }

        if let Some((region, alloc_start)) = found {
            let alloc_end = alloc_start.checked_add(size).expect("overflow");
            let excess_size = region.end_addr() - alloc_end;
            if excess_size > 0 {
                unsafe { self.add_free_region(alloc_end as *mut u8, excess_size) };
            }
            alloc_start as *mut u8
        } else {
            self.fallback.alloc(layout)
        }
    }

    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
        let (size, _) = Self::size_align(layout);
        self.add_free_region(ptr, size);
    }
}

unsafe impl<Fallback: Allocator> AllocatorAddFreeRegion for LinkedListAllocator<Fallback> {
    unsafe fn add_free_region(&mut self, ptr: *mut u8, size: usize) -> bool {
        let aligned_ptr = align_up(ptr as usize, core::mem::align_of::<ListNode>());
        if aligned_ptr != ptr as usize || size < core::mem::size_of::<ListNode>() {
            return false;
        }

        let mut node = ListNode::new(size);
        node.next = self.head.next.take();
        let node_ptr = ptr as *mut ListNode;
        node_ptr.write_volatile(node);
        self.head.next = Some(&mut *node_ptr);
        true
    }
}
