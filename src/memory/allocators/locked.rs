use crate::memory::allocators::Allocator;
use core::alloc::{GlobalAlloc, Layout};
use spin::{Mutex, MutexGuard};

pub struct Locked<T> {
    inner: Mutex<T>,
}

impl<T> Locked<T> {
    pub const fn new(inner: T) -> Self {
        Locked {
            inner: Mutex::new(inner),
        }
    }

    pub fn lock(&self) -> MutexGuard<T> {
        // TODO: multiprocessing
        self.inner.try_lock().expect("Tried to lock while locked")
    }
}

unsafe impl<T: Allocator> GlobalAlloc for Locked<T> {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        self.lock().alloc(layout)
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        self.lock().dealloc(ptr, layout)
    }
}
