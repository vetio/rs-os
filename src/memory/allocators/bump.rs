use crate::memory::allocators::{align_up, Allocator, AllocatorInRange};
use core::alloc::Layout;
use core::ops::Range;
use core::ptr::null_mut;

#[derive(Debug)]
pub struct BumpAllocator {
    heap_start: usize,
    heap_end: usize,
    next: usize,
    allocations: usize,
}

impl BumpAllocator {
    pub const fn empty() -> Self {
        BumpAllocator {
            heap_start: 0,
            heap_end: 0,
            next: 0,
            allocations: 0,
        }
    }

    pub unsafe fn with_region(region: &'static mut [u8]) -> Self {
        let heap_start = region.as_ptr() as usize;
        let heap_end = heap_start + region.len();
        BumpAllocator {
            heap_start,
            heap_end,
            next: heap_start,
            allocations: 0,
        }
    }

    pub unsafe fn with_ptr_range(&mut self, heap_range: Range<*mut u8>) {
        let size = heap_range.end.offset_from(heap_range.start);
        debug_assert!(size > 0);
        self.init(heap_range.start as usize, size as usize);
    }

    pub unsafe fn init(&mut self, heap_start: usize, heap_size: usize) {
        debug_assert_eq!(0, self.allocations);
        self.heap_start = heap_start;
        self.heap_end = heap_start + heap_size;
        self.next = heap_start;
    }
}

unsafe impl Allocator for BumpAllocator {
    fn alloc(&mut self, layout: Layout) -> *mut u8 {
        let size = layout.size();
        let align = layout.align();

        let alloc_start = align_up(self.next, align);
        let alloc_end = match alloc_start.checked_add(size) {
            None => return null_mut(),
            Some(end) => end,
        };

        if alloc_end > self.heap_end {
            null_mut()
        } else {
            self.next = alloc_end;
            self.allocations += 1;
            alloc_start as *mut u8
        }
    }

    unsafe fn dealloc(&mut self, _ptr: *mut u8, _layout: Layout) {
        self.allocations -= 1;
        if self.allocations == 0 {
            self.next = self.heap_start;
        }
    }
}

unsafe impl AllocatorInRange for BumpAllocator {
    fn in_range(&self, ptr: *mut u8, _layout: Layout) -> bool {
        let ptr = ptr as usize;
        self.heap_start <= ptr && self.heap_end >= ptr
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test_case]
    fn empty_allocator_returns_null() {
        let mut alloc = BumpAllocator::empty();
        let u8_ptr = alloc.alloc(Layout::new::<u8>());
        assert!(u8_ptr.is_null());
    }

    #[test_case]
    fn allocate_u8() {
        let mut mem = [0u8; 2];
        let mut alloc = BumpAllocator::empty();
        unsafe { alloc.with_ptr_range(mem.as_mut_ptr_range()) };
        let u8_ptr1 = alloc.alloc(Layout::new::<u8>());
        let u8_ptr2 = alloc.alloc(Layout::new::<u8>());
        assert!(!u8_ptr1.is_null());
        assert!(!u8_ptr2.is_null());
        assert_ne!(u8_ptr1, u8_ptr2);
        unsafe {
            u8_ptr1.write_volatile(42);
            u8_ptr2.write_volatile(13);
        }
        assert_eq!(42, unsafe { *u8_ptr1 });
        assert_eq!(13, unsafe { *u8_ptr2 });
    }

    #[test_case]
    fn allocate_u8_full() {
        let mut mem = [0u8; 1];
        let mut alloc = BumpAllocator::empty();
        unsafe { alloc.with_ptr_range(mem.as_mut_ptr_range()) };
        let u8_ptr1 = alloc.alloc(Layout::new::<u8>());
        let u8_ptr2 = alloc.alloc(Layout::new::<u8>());
        assert!(!u8_ptr1.is_null());
        assert!(u8_ptr2.is_null());
    }

    #[test_case]
    fn allocate_u32() {
        let mut mem = [0u8; 12];
        let mut alloc = BumpAllocator::empty();
        unsafe { alloc.with_ptr_range(mem.as_mut_ptr_range()) };
        let u32_ptr1 = alloc.alloc(Layout::new::<u32>()) as *mut u32;
        let u32_ptr2 = alloc.alloc(Layout::new::<u32>()) as *mut u32;
        assert!(!u32_ptr1.is_null());
        assert!(!u32_ptr2.is_null());
        assert_ne!(u32_ptr1, u32_ptr2);
        unsafe {
            u32_ptr1.write_volatile(0xa1b2c3d4);
            u32_ptr2.write_volatile(13);
        }
        assert_eq!(0xa1b2c3d4, unsafe { *u32_ptr1 });
        assert_eq!(13, unsafe { *u32_ptr2 });
    }

    #[test_case]
    fn reset_when_empty() {
        let mut mem = [0u8; 2];
        let mut alloc = BumpAllocator::empty();
        unsafe { alloc.with_ptr_range(mem.as_mut_ptr_range()) };
        let layout = Layout::new::<u8>();

        // Fill up buffer
        let u8_ptr1 = alloc.alloc(layout);
        let u8_ptr2 = alloc.alloc(layout);
        let u8_ptr3 = alloc.alloc(layout);
        assert!(!u8_ptr1.is_null());
        assert!(!u8_ptr2.is_null());
        assert!(u8_ptr3.is_null());

        // Clear buffer
        unsafe { alloc.dealloc(u8_ptr1, layout) };
        unsafe { alloc.dealloc(u8_ptr2, layout) };

        // Can now allocate 2 bytes again
        let u8_ptr4 = alloc.alloc(layout);
        let u8_ptr5 = alloc.alloc(layout);
        assert!(!u8_ptr4.is_null());
        assert!(!u8_ptr5.is_null());
    }
}
