use core::alloc::Layout;

pub mod bump;
pub mod chained;
pub mod fixed_block;
pub mod linked_list;
pub mod locked;
pub mod static_mem;

pub unsafe trait Allocator {
    fn alloc(&mut self, layout: Layout) -> *mut u8;

    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout);
}

pub unsafe trait AllocatorInRange: Allocator {
    fn in_range(&self, ptr: *mut u8, layout: Layout) -> bool;
}

pub unsafe trait AllocatorAddFreeRegion: Allocator {
    unsafe fn add_free_region(&mut self, ptr: *mut u8, size: usize) -> bool;
}

pub fn align_up(addr: usize, align: usize) -> usize {
    (addr + align - 1) & !(align - 1)
}

#[cfg(test)]
mod test {}
