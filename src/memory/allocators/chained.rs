use crate::memory::allocators::{Allocator, AllocatorAddFreeRegion, AllocatorInRange};
use core::alloc::Layout;

pub struct ChainedAllocator<A1, A2> {
    pub allocator_1: A1,
    pub allocator_2: A2,
}

impl<A1, A2> ChainedAllocator<A1, A2> {
    pub const fn new(allocator_1: A1, allocator_2: A2) -> Self {
        ChainedAllocator {
            allocator_1,
            allocator_2,
        }
    }
}

unsafe impl<A1: AllocatorInRange, A2: AllocatorAddFreeRegion> Allocator
    for ChainedAllocator<A1, A2>
{
    fn alloc(&mut self, layout: Layout) -> *mut u8 {
        let ptr = self.allocator_1.alloc(layout);
        if ptr.is_null() {
            self.allocator_2.alloc(layout)
        } else {
            ptr
        }
    }

    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
        if self.allocator_1.in_range(ptr, layout) {
            if !self.allocator_2.add_free_region(ptr, layout.size()) {
                self.allocator_1.dealloc(ptr, layout);
            }
        } else {
            self.allocator_2.dealloc(ptr, layout);
        }
    }
}
