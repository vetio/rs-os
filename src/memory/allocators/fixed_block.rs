use crate::memory::allocators::linked_list::LinkedListAllocator;
use crate::memory::allocators::static_mem::StaticMemoryAllocator;
use crate::memory::allocators::Allocator;
use crate::memory::page_allocator::PageAllocateError;
use crate::{FRAME_ALLOCATOR, PAGE_ALLOCATOR};
use core::alloc::Layout;
use core::mem::MaybeUninit;
use core::ptr::NonNull;
use x86_64::structures::paging::{FrameAllocator, Page, Size4KiB};
use x86_64::{align_up, VirtAddr};

struct FixedBlockNode {
    next: Option<&'static mut FixedBlockNode>,
}

struct FixedBlockPage {
    layout: Layout,
    page: Page,
    unused_offset: u64,
    free: Option<&'static mut FixedBlockNode>,
}

impl FixedBlockPage {
    fn new(layout: Layout) -> Result<Self, FixedBlockAllocateError> {
        let page = PAGE_ALLOCATOR
            .get()
            .ok_or(FixedBlockAllocateError::PageAllocatorNotInitialized)?
            .lock()
            .alloc_page_4kb()
            .map_err(FixedBlockAllocateError::AllocateControlStructure)?;
        Ok(FixedBlockPage {
            layout,
            page,
            unused_offset: 0,
            free: None,
        })
    }

    fn unused_addr(&self) -> u64 {
        (self.page.start_address() + self.unused_offset).as_u64()
    }

    fn has_free_block(&mut self) -> bool {
        self.free.is_some()
            || align_up(self.unused_offset, self.layout.align() as u64)
            <= (self.page.size() - self.layout.size() as u64)
    }

    fn alloc_block(&mut self) -> Result<NonNull<u8>, FixedBlockAllocateError> {
        if self.free.is_some() {
            let free = self.free.take().unwrap();
            self.free = free.next.take();
            return Ok(NonNull::from(free).cast());
        }
        let alloc_start = (self.page.start_address() + self.unused_offset).as_u64();
        let alloc_start = align_up(alloc_start, self.layout.align() as u64);
        let alloc_end = alloc_start + self.layout.size() as u64;
        if alloc_end > (self.page.start_address() + self.page.size()).as_u64() {
            return Err(FixedBlockAllocateError::PageEmpty);
        }
        self.unused_offset = alloc_end;
        return Ok(NonNull::new(alloc_start as *mut u8).unwrap());
    }
}

struct FixedBlockClass {
    last_num_pages: u64,
    pages: &'static mut [Option<FixedBlockPage>],
    layout: Layout,
}

impl FixedBlockClass {
    fn new(layout: Layout) -> Self {
        FixedBlockClass {
            last_num_pages: 0,
            pages: unsafe { core::slice::from_raw_parts_mut(NonNull::dangling().as_ptr(), 0) },
            layout,
        }
    }

    fn find_page_with_empty_slot(
        &mut self,
    ) -> Result<&mut FixedBlockPage, FixedBlockAllocateError> {
        let mut page_index = None;
        for i in 0..self.pages.len() {
            match &mut self.pages[i] {
                Some(page) => {
                    if page.has_free_block() {
                        page_index = Some(i);
                        break;
                    }
                }
                _ => {}
            }
        }
        if page_index.is_none() {
            for i in 0..self.pages.len() {
                match &mut self.pages[i] {
                    e if e.is_none() => {
                        *e = Some(FixedBlockPage::new(self.layout)?);
                        page_index = Some(i);
                        break;
                    }
                    _ => {}
                }
            }
        }
        if page_index.is_none() {
            let page = PAGE_ALLOCATOR
                .get()
                .ok_or(FixedBlockAllocateError::PageAllocatorNotInitialized)?
                .lock()
                .alloc_page_range_4kb(self.last_num_pages + 1)
                .map_err(FixedBlockAllocateError::AllocateControlStructure)?;
            let page_array_ptr = page.start.start_address().as_mut_ptr::<Option<FixedBlockPage>>();
            let page_array_len =
                page.map(|p| p.size()).sum::<u64>() / core::mem::size_of::<Option<FixedBlockPage>>() as u64;
            let new_pages = unsafe {
                core::ptr::slice_from_raw_parts_mut(page_array_ptr, page_array_len as usize)
                    .as_uninit_slice_mut()
                    .unwrap()
            };
            for (i, old_page) in self.pages.iter_mut().enumerate() {
                unsafe { new_pages[i].as_mut_ptr().write(old_page.take()) };
            }
            for i in self.pages.len()..page_array_len as usize {
                unsafe { new_pages[i].as_mut_ptr().write(None) };
            }
            let block_page = FixedBlockPage::new(self.layout);
            unsafe {
                new_pages[self.pages.len()]
                    .as_mut_ptr()
                    .write(Some(block_page?))
            };
            page_index = Some(self.pages.len());
            self.pages = unsafe { core::mem::transmute(new_pages) };
            self.last_num_pages += 1;
        }

        Ok(self.pages[page_index.unwrap()].as_mut().unwrap())
    }
}

pub struct FixedBlockAllocator<FB> {
    block_classes: Option<&'static mut [Option<FixedBlockClass>]>,
    fallback: FB,
}

impl FixedBlockAllocator<LinkedListAllocator<StaticMemoryAllocator>> {
    pub const fn new() -> Self {
        FixedBlockAllocator::with_fallback(
            LinkedListAllocator::with_fallback(
                StaticMemoryAllocator
            )
        )
    }
}

impl<FB> FixedBlockAllocator<FB> {
    pub const fn with_fallback(fallback: FB) -> Self {
        FixedBlockAllocator {
            block_classes: None,
            fallback,
        }
    }
}

pub enum FixedBlockAllocateError {
    AllocateControlStructure(PageAllocateError),
    TooLarge,
    PageEmpty,
    PageAllocatorNotInitialized,
}

impl<FB: Allocator> FixedBlockAllocator<FB> {
    fn block_classes(&mut self) -> Result<&mut [Option<FixedBlockClass>], FixedBlockAllocateError> {
        if self.block_classes.is_none() {
            let page = PAGE_ALLOCATOR
                .get()
                .ok_or(FixedBlockAllocateError::PageAllocatorNotInitialized)?
                .lock()
                .alloc_page_4kb()
                .map_err(FixedBlockAllocateError::AllocateControlStructure)?;
            let block_class_ptr = page.start_address().as_mut_ptr::<Option<FixedBlockClass>>();
            let num_block_classes =
                page.size() as usize / core::mem::size_of::<Option<FixedBlockClass>>();
            let block_classes = unsafe {
                core::ptr::slice_from_raw_parts_mut(block_class_ptr, num_block_classes)
                    .as_uninit_slice_mut()
                    .unwrap()
            };
            for bc in block_classes.iter_mut() {
                unsafe { bc.as_mut_ptr().write(None) };
            }
            self.block_classes = Some(unsafe { core::mem::transmute(block_classes) });
        }
        Ok(self.block_classes.as_mut().unwrap())
    }

    fn alloc_block_classes(
        &mut self,
        len: usize,
    ) -> Option<&'static mut [MaybeUninit<Option<FixedBlockClass>>]> {
        todo!();
        let size = len * core::mem::size_of::<Option<FixedBlockClass>>();
        let align = core::mem::align_of::<Option<FixedBlockClass>>();
        let ptr = self
            .fallback
            .alloc(Layout::from_size_align(size, align).unwrap());
        if ptr.is_null() {
            None
        } else {
            unsafe {
                core::ptr::slice_from_raw_parts_mut(ptr as *mut Option<FixedBlockClass>, len)
                    .as_uninit_slice_mut()
            }
        }
    }

    fn find_block_class(
        &mut self,
        layout: Layout,
    ) -> Result<&mut FixedBlockClass, FixedBlockAllocateError> {
        let block_classes = self.block_classes()?;
        let mut bc_index = block_classes
            .iter_mut()
            .enumerate()
            .filter_map(|(i, bc)| bc.as_mut().map(|bc| (i, bc)))
            .find(|(_, bc)| bc.layout == layout)
            .map(|(i, _)| i);
        if bc_index == None {
            bc_index = block_classes
                .iter_mut()
                .enumerate()
                .find(|(_, bc)| bc.is_none())
                .map(|(i, _)| i);
        }
        if bc_index == None {
            todo!("grow block classes");
        }

        let bc_index = bc_index.unwrap();

        if block_classes[bc_index].is_none() {
            block_classes[bc_index] = Some(FixedBlockClass::new(layout));
        }

        Ok(block_classes[bc_index].as_mut().unwrap())
    }

    fn ensure_layout_limit(layout: Layout) -> Result<Layout, FixedBlockAllocateError> {
        if layout.size() > 256 {
            Err(FixedBlockAllocateError::TooLarge)
        } else {
            Ok(layout
                .align_to(core::mem::align_of::<FixedBlockNode>())
                .expect("alignment adjusting failed")
                .pad_to_align())
        }
    }

    fn find_empty_slot(&mut self, layout: Layout) -> Result<NonNull<u8>, FixedBlockAllocateError> {
        let layout = Self::ensure_layout_limit(layout)?;

        let block_class = self.find_block_class(layout)?;
        let page = block_class.find_page_with_empty_slot()?;
        page.alloc_block()
    }

    fn find_page_for_ptr(
        &mut self,
        ptr: *mut u8,
        layout: Layout,
    ) -> Result<Option<&mut FixedBlockPage>, FixedBlockAllocateError> {
        let layout = Self::ensure_layout_limit(layout)?;

        let ptr_page = Page::<Size4KiB>::containing_address(VirtAddr::from_ptr(ptr));

        let block_class = self.find_block_class(layout)?;
        for page in block_class.pages.iter_mut().flatten() {
            if page.page == ptr_page {
                return Ok(Some(page));
            }
        }
        Ok(None)
    }
}

unsafe impl<FB: Allocator> Allocator for FixedBlockAllocator<FB> {
    fn alloc(&mut self, layout: Layout) -> *mut u8 {
        match self.find_empty_slot(layout) {
            Ok(ptr) => ptr.as_ptr(),
            Err(_) => self.fallback.alloc(layout),
        }
    }

    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
        match self.find_page_for_ptr(ptr, layout) {
            Ok(Some(page)) => {
                assert!(page.layout.size() >= core::mem::size_of::<FixedBlockNode>());
                let ptr = ptr as *mut FixedBlockNode;
                let next = page.free.take();
                let free_node = FixedBlockNode { next };
                unsafe { ptr.write(free_node) };
                page.free = Some(unsafe { &mut *ptr });
            }
            _ => self.fallback.dealloc(ptr, layout),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::memory::allocators::fixed_block::FixedBlockAllocator;
    use crate::memory::allocators::Allocator;
    use core::alloc::Layout;
    use core::ptr::null_mut;

    struct EmptyAllocator;

    unsafe impl Allocator for EmptyAllocator {
        fn alloc(&mut self, layout: Layout) -> *mut u8 {
            null_mut()
        }

        unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
            panic!("EmptyAllocator::dealloc should never be called");
        }
    }

    #[test_case]
    fn alloc_large_uses_fallback() {
        let mut alloc = FixedBlockAllocator::with_fallback(EmptyAllocator);
        let ptr = alloc.alloc(Layout::new::<[u8; 1024]>());
        assert!(ptr.is_null());
    }

    #[test_case]
    fn alloc_u8() {
        let mut alloc = FixedBlockAllocator::with_fallback(EmptyAllocator);
        let layout = Layout::new::<u8>();
        let ptr = alloc.alloc(layout);
        assert!(!ptr.is_null());
        unsafe { ptr.write_volatile(42) };
        unsafe { alloc.dealloc(ptr, layout) };
    }
}
