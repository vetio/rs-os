use crate::memory::allocators::bump::BumpAllocator;
use crate::memory::allocators::locked::Locked;
use crate::memory::allocators::Allocator;
use core::alloc::Layout;
use spin::Mutex;

lazy_static::lazy_static! {
    static ref ALLOC: Locked<BumpAllocator> = {
        static mut MEMORY: [u8; 8000] = [0; 8000];
        Locked::new(unsafe { BumpAllocator::with_region( &mut MEMORY ) })
    };
}

pub struct StaticMemoryAllocator;

unsafe impl Allocator for StaticMemoryAllocator {
    fn alloc(&mut self, layout: Layout) -> *mut u8 {
        ALLOC.lock().alloc(layout)
    }

    unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
        ALLOC.lock().dealloc(ptr, layout);
    }
}
