use crate::FRAME_ALLOCATOR;
use bootloader::BootInfo;
use x86_64::registers::control::Cr3;
use x86_64::structures::paging::mapper::PhysToVirt;
use x86_64::structures::paging::page::PageRangeInclusive;
use x86_64::structures::paging::page_table::PageTableEntry;
use x86_64::structures::paging::{FrameAllocator, Page, PageTable, PageTableFlags, PageTableIndex, PhysFrame, Size4KiB, PageSize};
use x86_64::{PhysAddr, VirtAddr};

const NUM_PAGE_ENTRIES: u16 = 512;

#[derive(Debug, Copy, Clone)]
struct MemoryOffset(VirtAddr);

impl PhysToVirt for MemoryOffset {
    fn phys_to_virt(&self, phys_frame: PhysFrame<Size4KiB>) -> *mut PageTable {
        (self.0 + phys_frame.start_address().as_u64()).as_mut_ptr()
    }
}

pub struct PageAllocator<'a> {
    level_4_page_table: &'a mut PageTable,
    memory_offset: MemoryOffset,
}

#[derive(Debug)]
pub enum PageAllocateError {
    NoFreePage,
    NoFrameAllocated,
}

impl PageAllocator<'static> {
    pub unsafe fn with_active_table(boot_info: &'static BootInfo) -> Self {
        let memory_offset = VirtAddr::new(boot_info.physical_memory_offset);
        let page_table = active_level_4_table(memory_offset);
        Self::with_table(page_table, memory_offset)
    }
}

impl<'a> PageAllocator<'a> {
    pub unsafe fn with_table(l4_page_table: &'a mut PageTable, memory_offset: VirtAddr) -> Self {
        PageAllocator {
            level_4_page_table: l4_page_table,
            memory_offset: MemoryOffset(memory_offset),
        }
    }

    pub fn virt_to_phys(&self, virt: VirtAddr) -> Option<PhysAddr> {
        let p4_entry = &self.level_4_page_table[virt.p4_index()];

        let p3_table_ptr = self.memory_offset.phys_to_virt(p4_entry.frame().ok()?);
        let p3_table = unsafe { &*p3_table_ptr };
        let p3_entry = &p3_table[virt.p3_index()];

        let p2_table_ptr = self.memory_offset.phys_to_virt(p3_entry.frame().ok()?);
        let p2_table = unsafe { &*p2_table_ptr };
        let p2_entry = &p2_table[virt.p2_index()];

        let p1_table_ptr = self.memory_offset.phys_to_virt(p2_entry.frame().ok()?);
        let p1_table = unsafe { &*p1_table_ptr };
        let p1_entry = &p1_table[virt.p1_index()];
        if !p1_entry.flags().contains(PageTableFlags::PRESENT) {
            return None;
        }

        Some(p1_entry.addr() + usize::from(virt.page_offset()))
    }

    fn walk_p1_entries(&mut self, mut f: impl FnMut(Page, &mut PageTableEntry) -> bool) {
        for p4_index in (0..NUM_PAGE_ENTRIES).map(PageTableIndex::new) {
            let p3_table = match unsafe {
                self.level_4_page_table
                    .get_sub_table(p4_index, self.memory_offset)
            } {
                Some(table) => table,
                None => continue,
            };
            for p3_index in (0..NUM_PAGE_ENTRIES).map(PageTableIndex::new) {
                let p2_table = match unsafe { p3_table.get_sub_table(p3_index, self.memory_offset) }
                {
                    Some(table) => table,
                    None => continue,
                };
                for p2_index in (0..NUM_PAGE_ENTRIES).map(PageTableIndex::new) {
                    let p1_table =
                        match unsafe { p2_table.get_sub_table(p2_index, self.memory_offset) } {
                            Some(table) => table,
                            None => continue,
                        };
                    for p1_index in (0..NUM_PAGE_ENTRIES).map(PageTableIndex::new) {
                        let entry = &mut p1_table[p1_index];
                        let page = Page::from_page_table_indices(
                            p4_index.into(),
                            p3_index.into(),
                            p2_index.into(),
                            p1_index.into(),
                        );
                        if !f(page, entry) {
                            return;
                        }
                    }
                }
            }
        }
    }

    fn try_find_empty_page_range(&mut self, num_pages: u64) -> Option<PageRangeInclusive> {
        assert!(num_pages > 0);
        let mut first_page = None;
        let mut last_page = None;
        let mut num_found_pages = 0;

        let mut prev_page = Page::containing_address(VirtAddr::new(0));
        self.walk_p1_entries(|page, entry| {
            if !entry.is_unused() || (page - prev_page) != 1 {
                first_page = None;
                last_page = None;
                num_found_pages = 0;
            } else {
                num_found_pages += 1;
                if first_page.is_none() {
                    first_page = Some(page);
                }
                if num_found_pages == num_pages {
                    last_page = Some(page);
                    return false;
                }
            }
            prev_page = page;
            true
        });
        match (first_page, last_page) {
            (Some(first_page), Some(last_page)) => {
                Some(Page::range_inclusive(first_page, last_page))
            }
            _ => None,
        }
    }

    fn alloc_p1_table(&mut self) -> Result<Page, PageAllocateError> {
        for p4_index in (0..NUM_PAGE_ENTRIES).map(PageTableIndex::new) {
            let p3_table = match unsafe {
                self.level_4_page_table
                    .get_sub_table(p4_index, self.memory_offset)
            } {
                Some(table) => table,
                None => continue,
            };
            for p3_index in (0..NUM_PAGE_ENTRIES).map(PageTableIndex::new) {
                let p2_table = match unsafe { p3_table.get_sub_table(p3_index, self.memory_offset) }
                {
                    Some(table) => table,
                    None => continue,
                };
                let p2_index = match unsafe { p2_table.try_alloc_sub_table(self.memory_offset) } {
                    Ok((index, _)) => index,
                    Err(PageAllocateError::NoFreePage) => continue,
                    Err(e) => return Err(e),
                };
                let first_page = Page::from_page_table_indices(
                    p4_index,
                    p3_index,
                    p2_index,
                    PageTableIndex::new(0),
                );
                return Ok(first_page);
            }

            let (p3_index, p2_table) =
                match unsafe { p3_table.try_alloc_sub_table(self.memory_offset) } {
                    Ok((index, table)) => (index, table),
                    Err(PageAllocateError::NoFreePage) => continue,
                    Err(e) => return Err(e),
                };
            let (p2_index, _) = unsafe { p2_table.try_alloc_sub_table(self.memory_offset)? };
            let first_page =
                Page::from_page_table_indices(p4_index, p3_index, p2_index, PageTableIndex::new(0));
            return Ok(first_page);
        }

        let (p4_index, p3_table) = unsafe {
            self.level_4_page_table
                .try_alloc_sub_table(self.memory_offset)?
        };
        let (p3_index, p2_table) = unsafe { p3_table.try_alloc_sub_table(self.memory_offset)? };
        let (p2_index, _) = unsafe { p2_table.try_alloc_sub_table(self.memory_offset)? };
        let first_page =
            Page::from_page_table_indices(p4_index, p3_index, p2_index, PageTableIndex::new(0));
        Ok(first_page)
    }

    fn alloc_frames(&mut self, page_range: PageRangeInclusive) -> Result<(), PageAllocateError> {
        for page in page_range {
            let p3_table = unsafe {
                self.level_4_page_table
                    .get_sub_table(page.p4_index(), self.memory_offset)
            }
            .unwrap();
            let p2_table =
                unsafe { p3_table.get_sub_table(page.p3_index(), self.memory_offset) }.unwrap();
            let p1_table =
                unsafe { p2_table.get_sub_table(page.p2_index(), self.memory_offset) }.unwrap();
            let entry = &mut p1_table[page.p1_index()];
            assert!(entry.is_unused());
            let frame = FRAME_ALLOCATOR
                .get()
                .unwrap()
                .lock()
                .allocate_frame()
                .ok_or(PageAllocateError::NoFrameAllocated)?;
            let flags = PageTableFlags::WRITABLE | PageTableFlags::PRESENT;
            entry.set_frame(frame, flags);
        }
        Ok(())
    }

    fn alloc_memory_mapped_register_frames(&mut self, page_range: PageRangeInclusive, base_address: u64) -> Result<(), PageAllocateError> {
        let mut phys_addr = PhysAddr::new(base_address);
        for page in page_range {
            let p3_table = unsafe {
                self.level_4_page_table
                    .get_sub_table(page.p4_index(), self.memory_offset)
            }
                .unwrap();
            let p2_table =
                unsafe { p3_table.get_sub_table(page.p3_index(), self.memory_offset) }.unwrap();
            let p1_table =
                unsafe { p2_table.get_sub_table(page.p2_index(), self.memory_offset) }.unwrap();
            let entry = &mut p1_table[page.p1_index()];
            assert!(entry.is_unused());
            let frame = PhysFrame::containing_address(phys_addr);
            assert_eq!(phys_addr, frame.start_address());
            let flags = PageTableFlags::WRITABLE | PageTableFlags::PRESENT |  PageTableFlags::NO_CACHE;
            entry.set_frame(frame, flags);
            phys_addr += Size4KiB::SIZE;
        }
        Ok(())
    }

    fn find_or_alloc_p1_range(&mut self, num_pages: u64) -> Result<PageRangeInclusive, PageAllocateError> {
        assert!(num_pages <= NUM_PAGE_ENTRIES as u64);
        match self.try_find_empty_page_range(num_pages) {
            Some(range) => Ok(range),
            None => {
                let first_page = self.alloc_p1_table()?;
                Ok(Page::range_inclusive(first_page, first_page + num_pages))
            }
        }
    }

    pub fn alloc_page_range_4kb(
        &mut self,
        num_pages: u64,
    ) -> Result<PageRangeInclusive, PageAllocateError> {
        let page_range = self.find_or_alloc_p1_range(num_pages)?;
        self.alloc_frames(page_range)?;
        Ok(page_range)
    }

    pub fn alloc_page_4kb(&mut self) -> Result<Page, PageAllocateError> {
        self.alloc_page_range_4kb(1).map(|range| range.start)
    }

    pub fn alloc_memory_mapped_register_range(&mut self, base_address: u64, num_pages: u64) -> Result<PageRangeInclusive, PageAllocateError> {
        let page_range = self.find_or_alloc_p1_range(num_pages)?;
        self.alloc_memory_mapped_register_frames(page_range, base_address)?;
        Ok(page_range)
    }

    pub fn alloc_memory_mapped_registers(&mut self, base_address: u64) -> Result<Page, PageAllocateError> {
        self.alloc_memory_mapped_register_range(base_address, 1).map(|range| range.start)
    }
}

trait PageTableExt {
    unsafe fn get_sub_table(
        &mut self,
        index: impl Into<PageTableIndex>,
        phys_to_virt: impl PhysToVirt,
    ) -> Option<&mut PageTable>;
    unsafe fn try_alloc_sub_table(
        &mut self,
        phys_to_virt: impl PhysToVirt,
    ) -> Result<(PageTableIndex, &mut PageTable), PageAllocateError>;
}
impl PageTableExt for PageTable {
    unsafe fn get_sub_table(
        &mut self,
        index: impl Into<PageTableIndex>,
        phys_to_virt: impl PhysToVirt,
    ) -> Option<&mut PageTable> {
        let entry = &mut self[index.into()];
        let frame = entry.frame().ok()?;
        let sub_table = phys_to_virt.phys_to_virt(frame);
        Some(&mut *sub_table)
    }

    unsafe fn try_alloc_sub_table(
        &mut self,
        phys_to_virt: impl PhysToVirt,
    ) -> Result<(PageTableIndex, &mut PageTable), PageAllocateError> {
        for (index, entry) in self.iter_mut().enumerate().skip(1) {
            if entry.is_unused() {
                let frame = FRAME_ALLOCATOR
                    .get()
                    .unwrap()
                    .lock()
                    .allocate_frame()
                    .ok_or(PageAllocateError::NoFrameAllocated)?;
                let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
                let page_table = &mut *phys_to_virt.phys_to_virt(frame);
                page_table.zero();
                entry.set_frame(frame, flags);
                return Ok((PageTableIndex::new(index as u16), page_table));
            }
        }
        Err(PageAllocateError::NoFreePage)
    }
}

/// Returns a mutable reference to the active level 4 page table.
///
/// # SAFETY
/// physical_memory_offset must map to the memory region mapping the physical memory in virtual
/// address space.
unsafe fn active_level_4_table(physical_memory_offset: VirtAddr) -> &'static mut PageTable {
    let (level_4_table_frame, _) = Cr3::read();

    let phys = level_4_table_frame.start_address();
    let virt = physical_memory_offset + phys.as_u64();
    let page_table_ptr: *mut PageTable = virt.as_mut_ptr();

    &mut *page_table_ptr
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::vga::VGA_TEXT_BUFFER;
    use crate::PAGE_ALLOCATOR;

    #[test_case]
    fn virt_to_phys_null() {
        let null = VirtAddr::new(0);
        let null_phys_addr = PAGE_ALLOCATOR.get().unwrap().lock().virt_to_phys(null);
        assert!(null_phys_addr.is_none());
    }

    #[test_case]
    fn virt_to_phys_vga() {
        let vga_addr = VirtAddr::new(VGA_TEXT_BUFFER as u64);
        let vga_phys_addr = PAGE_ALLOCATOR
            .get()
            .unwrap()
            .lock()
            .virt_to_phys(vga_addr)
            .expect("failed to resolve vga buffer PhysAddr");
        assert_eq!(VGA_TEXT_BUFFER as u64, vga_phys_addr.as_u64());
    }

    #[test_case]
    fn alloc_single_page() {
        let mut page_allocator = PAGE_ALLOCATOR.get().unwrap().lock();
        let page = page_allocator
            .alloc_page_4kb()
            .expect("Failed to allocate page");
        assert_ne!(
            VirtAddr::new(0),
            page.start_address(),
            "allocated null page"
        );
        let data_ptr = page.start_address().as_mut_ptr::<u32>();
        unsafe { data_ptr.write_volatile(42) };
    }

    #[test_case]
    fn alloc_ten_pages() {
        let mut page_allocator = PAGE_ALLOCATOR.get().unwrap().lock();
        let page_range = page_allocator
            .alloc_page_range_4kb(10)
            .expect("Failed to allocate page");
        assert_ne!(
            VirtAddr::new(0),
            page_range.start.start_address(),
            "allocated null page"
        );
        for page in page_range {
            let data_ptr = page.start_address().as_mut_ptr::<u32>();
            unsafe { data_ptr.write_volatile(42) };
        }
    }
}
