use crate::memory::allocators::fixed_block::FixedBlockAllocator;
use crate::memory::allocators::linked_list::LinkedListAllocator;
use crate::memory::allocators::locked::Locked;
use crate::memory::allocators::static_mem::StaticMemoryAllocator;
use core::alloc::Layout;

pub const HEAP_START: usize = 0x_4444_4444_0000;
pub const HEAP_SIZE: usize = 100 * 1024; // 100 KiB

#[global_allocator]
static ALLOCATOR: Locked<FixedBlockAllocator<LinkedListAllocator<StaticMemoryAllocator>>> =
    Locked::new(FixedBlockAllocator::new());

#[alloc_error_handler]
fn alloc_error(layout: Layout) -> ! {
    panic!("allocation error: {:?}", layout);
}
