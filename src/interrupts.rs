use crate::gdt::DOUBLE_FAULT_IST_INDEX;
use crate::hlt_loop;
use crate::prelude::*;
use lazy_static::lazy_static;
use pic8259_simple::ChainedPics;
use spin::{Mutex, MutexGuard};
use x86_64::instructions::port::Port;
use x86_64::registers::control::Cr2;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame, PageFaultErrorCode};
use crate::platform::hpet::InterruptStatus;
use core::fmt;

pub const PIC_1_OFFSET: u8 = 32;
pub const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

pub static PICS: Mutex<ChainedPics> =
    Mutex::new(unsafe { ChainedPics::new(PIC_1_OFFSET, PIC_2_OFFSET) });

#[derive(Debug, Copy, Clone)]
#[repr(u8)]
pub enum InterruptIndex {
    Timer = PIC_1_OFFSET,
    Keyboard,
    HpetTimer,
}

impl InterruptIndex {
    pub fn as_u8(self) -> u8 {
        self as u8
    }

    pub fn as_usize(self) -> usize {
        usize::from(self.as_u8())
    }
}

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        idt.divide_error.set_handler_fn(divide_error_handler);
        idt.debug.set_handler_fn(debug_handler);
        idt.non_maskable_interrupt.set_handler_fn(non_maskable_interrupt_handler);
        idt.breakpoint.set_handler_fn(breakpoint_handler);
        idt.overflow.set_handler_fn(overflow_handler);
        idt.invalid_opcode.set_handler_fn(invalid_opcode_handler);
        idt.device_not_available.set_handler_fn(device_not_available_handler);
        unsafe {
            idt.double_fault
                .set_handler_fn(double_fault_handler)
                // .set_stack_index(DOUBLE_FAULT_IST_INDEX)
                ;
        }
        idt.invalid_tss.set_handler_fn(invalid_tss_handler);
        idt.segment_not_present.set_handler_fn(segment_not_present_handler);
        idt.stack_segment_fault.set_handler_fn(stack_segment_full_handler);
        idt.general_protection_fault.set_handler_fn(general_protection_fault_handler);
        idt.page_fault.set_handler_fn(page_fault_handler);
        idt.x87_floating_point.set_handler_fn(x87_floating_point_handler);
        idt.alignment_check.set_handler_fn(alignment_check_handler);
        idt.machine_check.set_handler_fn(machine_check_handler);
        idt.simd_floating_point.set_handler_fn(simd_handler);
        idt.security_exception.set_handler_fn(security_handler);
        idt[InterruptIndex::Timer.as_usize()].set_handler_fn(timer_interrupt_handler);
        idt[InterruptIndex::Keyboard.as_usize()].set_handler_fn(keyboard_interrupt_handler);
        idt[InterruptIndex::HpetTimer.as_usize()].set_handler_fn(hpet_timer_handler);
        idt
    };
}

pub fn init_idt() {
    IDT.load();
}

pub fn init_interrupts() {
    unsafe { PICS.lock().initialize() };

    let mut pic1_port = Port::<u8>::new(0x21);
    let mut pic2_port = Port::<u8>::new(0xa1);

    unsafe { pic2_port.write(0xFE) };
    unsafe { pic1_port.write(0xFE) };
}

extern "x86-interrupt" fn foo_handler(stack_frame: &mut InterruptStackFrame) {
    tracing::debug!("EXCEPTION: FOO\n{:#?}", stack_frame);
    interrupt_panic(format_args!("EXCEPTION: FOO"));
}

extern "x86-interrupt" fn breakpoint_handler(stack_frame: &mut InterruptStackFrame) {
    tracing::debug!("EXCEPTION: BREAKPOINT\n{:#?}", stack_frame);
}

extern "x86-interrupt" fn simd_handler(stack_frame: &mut InterruptStackFrame) {
    tracing::debug!("EXCEPTION: SIMD\n{:#?}", stack_frame);
}

extern "x86-interrupt" fn divide_error_handler(stack_frame: &mut InterruptStackFrame) {
    panic!("EXCEPTION: DIVIDE ERROR");
    // tracing::error!(
    //     "EXCEPTION: DIVIDE ERROR\n{:#?}",
    //     stack_frame
    // );
    // interrupt_panic(format_args!(
    //     "EXCEPTION: DIVIDE ERROR\n{:#?}",
    //     stack_frame
    // ))
}

extern "x86-interrupt" fn debug_handler(stack_frame: &mut InterruptStackFrame) {
    tracing::error!(
        "EXCEPTION: DEBUG\n{:#?}",
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: DEBUG\n{:#?}",
        stack_frame
    ))
}

extern "x86-interrupt" fn non_maskable_interrupt_handler(stack_frame: &mut InterruptStackFrame) {
    tracing::error!(
        "EXCEPTION: NON MASKABLE INTERRUPT\n{:#?}",
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: NON MASKABLE INTERRUPT\n{:#?}",
        stack_frame
    ))
}

extern "x86-interrupt" fn overflow_handler(stack_frame: &mut InterruptStackFrame) {
    tracing::error!(
        "EXCEPTION: OVERFLOW\n{:#?}",
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: OVERFLOW\n{:#?}",
        stack_frame
    ))
}

extern "x86-interrupt" fn invalid_opcode_handler(stack_frame: &mut InterruptStackFrame) {
    tracing::error!(
        "EXCEPTION: INVALID OPCODE\n{:#?}",
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: INVALID OPCODE\n{:#?}",
        stack_frame
    ))
}

extern "x86-interrupt" fn device_not_available_handler(stack_frame: &mut InterruptStackFrame) {
    tracing::error!(
        "EXCEPTION: INVALID OPCODE\n{:#?}",
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: INVALID OPCODE\n{:#?}",
        stack_frame
    ))
}

extern "x86-interrupt" fn double_fault_handler(
    stack_frame: &mut InterruptStackFrame,
    error_code: u64,
) -> ! {
    tracing::error!(
        "EXCEPTION: DOUBLE FAULT\n{} - {:#?}",
        error_code, stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: DOUBLE FAULT\n{} - {:#?}",
        error_code, stack_frame
    ))
}

extern "x86-interrupt" fn page_fault_handler(
    stack_frame: &mut InterruptStackFrame,
    error_code: PageFaultErrorCode,
) {
    tracing::error!(
        "EXCEPTION: PAGE FAULT\nAccessed Address: {:?}\nError Code: {:?}\n{:#?}",
        Cr2::read(),
        error_code,
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: PAGE FAULT\nAccessed Address: {:?}\nError Code: {:?}\n{:#?}",
        Cr2::read(),
        error_code,
        stack_frame
    ))
}

extern "x86-interrupt" fn invalid_tss_handler(
    stack_frame: &mut InterruptStackFrame,
    error_code: u64,
) {
    tracing::error!(
        "EXCEPTION: INVALID TSS\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: INVALID TSS\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    ))
}

extern "x86-interrupt" fn segment_not_present_handler(
    stack_frame: &mut InterruptStackFrame,
    error_code: u64,
) {
    tracing::error!(
        "EXCEPTION: SEGMENT NOT PRESENT\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: SEGMENT NOT PRESENT\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    ))
}

extern "x86-interrupt" fn stack_segment_full_handler(
    stack_frame: &mut InterruptStackFrame,
    error_code: u64,
) {
    tracing::error!(
        "EXCEPTION: STACK SEGMENT FULL\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: STACK SEGMENT FULL\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    ))
}

extern "x86-interrupt" fn general_protection_fault_handler(
    stack_frame: &mut InterruptStackFrame,
    error_code: u64,
) {
    tracing::error!(
        "EXCEPTION: GENERAL PROTECTION FAULT\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: GENERAL PROTECTION FAULT\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    ))
}

extern "x86-interrupt" fn alignment_check_handler(
    stack_frame: &mut InterruptStackFrame,
    error_code: u64,
) {
    tracing::error!(
        "EXCEPTION: ALIGNMENT CHECK\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: ALIGNMENT CHECK\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    ))
}

extern "x86-interrupt" fn machine_check_handler(
    stack_frame: &mut InterruptStackFrame,
) -> ! {
    tracing::error!(
        "EXCEPTION: MACHINE CHECK\n{:#?}",
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: MACHINE CHECK\n{:#?}",
        stack_frame
    ))
}

extern "x86-interrupt" fn security_handler(
    stack_frame: &mut InterruptStackFrame,
    error_code: u64,
) {
    tracing::error!(
        "EXCEPTION: SECURITY\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: SECURITY\nError Code: {:?}\n{:#?}",
        error_code,
        stack_frame
    ))
}

extern "x86-interrupt" fn virtualization_handler(
    stack_frame: &mut InterruptStackFrame
) {
    tracing::error!(
        "EXCEPTION: VIRTUALIZATION\n{:#?}",
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: VIRTUALIZATION\n{:#?}",
        stack_frame
    ))
}

extern "x86-interrupt" fn x87_floating_point_handler(stack_frame: &mut InterruptStackFrame) {
    tracing::error!(
        "EXCEPTION: X87 FLOATING POINT ERROR\n{:#?}",
        stack_frame
    );
    interrupt_panic(format_args!(
        "EXCEPTION: X87 FLOATING POINT ERROR\n{:#?}",
        stack_frame
    ))
}

extern "x86-interrupt" fn timer_interrupt_handler(stack_frame: &mut InterruptStackFrame) {
    unsafe {
        crate::platform::pit::tick();
        PICS.lock()
            .notify_end_of_interrupt(InterruptIndex::Timer.as_u8());
    }
}

extern "x86-interrupt" fn hpet_timer_handler(_stack_frame: &mut InterruptStackFrame) {
    tracing::trace!("HPET Timer");
    unsafe { crate::platform::hpet::tick() };
    unsafe { crate::platform::apic::notify_end_of_interrupt() };
}

extern "x86-interrupt" fn keyboard_interrupt_handler(stack_frame: &mut InterruptStackFrame) {
    use pc_keyboard::{layouts, DecodedKey, HandleControl, Keyboard, ScancodeSet1};

    tracing::trace!("Keyboard - {:?}", stack_frame);

    lazy_static! {
        static ref KEYBOARD: Mutex<Keyboard<layouts::Us104Key, ScancodeSet1>> = Mutex::new(
            Keyboard::new(layouts::Us104Key, ScancodeSet1, HandleControl::Ignore)
        );
    }
    ;

    let mut keyboard: MutexGuard<Keyboard<_, _>> = KEYBOARD.lock();
    let mut port = Port::new(0x60);

    let scancode: u8 = unsafe { port.read() };
    if let Ok(Some(key_event)) = keyboard.add_byte(scancode) {
        if let Some(key) = keyboard.process_keyevent(key_event) {
            match key {
                DecodedKey::Unicode(c) => crate::print!("{}", c),
                DecodedKey::RawKey(k) => crate::print!("{:?}", k),
            }
        }
    }

    unsafe {
        PICS.lock()
            .notify_end_of_interrupt(InterruptIndex::Keyboard.as_u8());
    }
}

fn interrupt_panic(args: fmt::Arguments) -> ! {
    crate::println!("Interrupt panicked: {}", args);
    hlt_loop()
}

#[cfg(test)]
mod test {
    #[test_case]
    fn test_breakpoint_exception_does_not_crash() {
        x86_64::instructions::interrupts::int3();
    }
}
