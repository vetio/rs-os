use crate::arbitrary::Arbitrary;
use core::marker::PhantomData;

pub trait Strategy {
    type Item;
}

struct Any<A> {
    _val: PhantomData<fn() -> A>,
}

impl<A> Strategy for Any<A> {
    type Item = A;
}

pub fn any<A: Arbitrary>() -> impl Strategy<Item = A> {
    Any {
        _val: PhantomData,
    }
}
