pub enum TestResult {
    Success,
    Ignored,
    Failed,
}
