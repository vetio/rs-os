#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

use crate::dwarf::{
    EhFrameHeader, EhFrameSearchTable, Instruction, InstructionReader, ParsedCie, ParsedFde,
    ParsedLsdaEntry,
};
use crate::hlt_loop;
use crate::native::context::{getcontext, setcontext, CpuContext};
use crate::panic::PanicPayload;
use alloc::boxed::Box;
use core::any::Any;
use core::ffi::c_void;
use core::intrinsics::exp2f32;
use x86_64::VirtAddr;

type c_int = i32;

#[derive(Debug, Copy, Clone)]
pub enum Expr {}

#[derive(Debug, Copy, Clone)]
pub enum RegisterRule {
    Undefined,
    SameValue,
    Offset(isize),
    ValOffset(isize),
    Register(u8),
    Expression(Expr),
    ValExpression(Expr),
    Architectural,
}

impl RegisterRule {}

#[derive(Debug)]
pub enum Cfa {
    Unset,
    RegisterOffset { register: u8, offset: i64 },
}

impl Cfa {}

#[derive(Debug)]
pub struct Location(usize);

#[derive(Debug)]
pub struct Processed {
    location: usize,
    cfa: Cfa,
    initial_registers: [RegisterRule; 67],
    registers: [RegisterRule; 67],
    return_addr_col: usize,
}

impl Processed {
    pub fn process(mut location: usize, fde: &ParsedFde) -> Self {
        let mut p = Processed {
            location: fde.initial_location(),
            cfa: Cfa::Unset,
            initial_registers: [RegisterRule::Undefined; 67],
            registers: [RegisterRule::Undefined; 67],
            return_addr_col: 0,
        };

        p.init(&mut location, fde.cie());

        p.process_int(&mut location, fde);

        p
    }

    fn init(&mut self, l1: &mut usize, cie: &ParsedCie) {
        // TODO: needed? libunwind does something like this
        // Preserved register
        for i in 0..17 {
            self.registers[i] = RegisterRule::SameValue;
        }

        self.return_addr_col = cie.return_address_register();

        self.process_instructions(l1, cie.code_alignment_factor(), cie.instructions());
    }

    fn process_int(&mut self, l1: &mut usize, fde: &ParsedFde) {
        self.initial_registers.copy_from_slice(&self.registers[..]);
        self.process_instructions(l1, fde.cie().code_alignment_factor(), fde.instructions());
    }

    fn process_instructions(
        &mut self,
        l1: &mut usize,
        code_alignment_factor: usize,
        reader: InstructionReader,
    ) {
        let mut l2 = self.location;
        use Instruction::*;
        for i in reader {
            match i {
                SetLoc { .. } => todo!(),
                AdvanceLoc { delta } => {
                    l2 += code_alignment_factor * (delta as usize);
                    if l2 > *l1 {
                        break;
                    }
                }
                DefCfa { register, offset } => self.cfa = Cfa::RegisterOffset { register, offset },
                DefCfaRegister { .. } => todo!(),
                DefCfaOffset { offset } => {
                    self.cfa = match self.cfa {
                        Cfa::Unset => panic!("No register set before"),
                        Cfa::RegisterOffset {
                            register,
                            offset: _olD_offset,
                        } => Cfa::RegisterOffset { register, offset },
                    }
                }
                DefCfaExpression { .. } => todo!(),
                Undefined { .. } => todo!(),
                SameValue { .. } => todo!(),
                Offset { register, offset } => {
                    self.registers[register as usize] = RegisterRule::Offset(offset as isize);
                }
                ValOffset { .. } => todo!(),
                Register { .. } => todo!(),
                Expression { .. } => todo!(),
                ValExpression { .. } => todo!(),
                Restore { register } => {
                    self.registers[register as usize] = self.initial_registers[register as usize];
                },
                RestoreExtended { .. } => todo!(),
                RememberState => todo!(),
                RestoreState => todo!(),
                Nop => {}
            }
        }
    }

    pub fn eval_cfa(&self, context: &CpuContext) -> u128 {
        match self.cfa {
            Cfa::Unset => panic!("cfa unset"),
            Cfa::RegisterOffset { register, offset } => {
                let base = self.eval_reg(register, context) as i64;
                (base + offset) as u128
            }
        }
    }

    pub fn reg_needs_update(&self, idx: u8) -> bool {
        match self.registers[idx as usize] {
            RegisterRule::Undefined | RegisterRule::SameValue => false,
            _ => true,
        }
    }

    pub fn eval_reg(&self, idx: u8, context: &CpuContext) -> u128 {
        match self.registers[idx as usize] {
            RegisterRule::Undefined => {
                panic!("Attempt to eval undefined register: {}\n{:?}", idx, self)
            }
            RegisterRule::SameValue => context.get_reg_value(idx),
            RegisterRule::Offset(offset) => {
                let cfa = self.eval_cfa(context) as isize;
                let addr = cfa + offset;
                let ptr = addr as *const u64;
                (unsafe { ptr.read_volatile() } as u128)
            }
            RegisterRule::ValOffset(offset) => {
                let cfa = self.eval_cfa(context) as isize;
                (cfa + offset) as u128
            }
            RegisterRule::Register(register) => context.get_reg_value(register),
            RegisterRule::Expression(_) => todo!("Expresion support"),
            RegisterRule::ValExpression(_) => todo!("Expresion support"),
            RegisterRule::Architectural => todo!("architectural: not sure what to do"),
        }
    }

    pub fn apply_to(&self, context: &mut CpuContext) {
        for idx in 0..self.registers.len() {
            if self.reg_needs_update(idx as u8) {
                let val = self.eval_reg(idx as u8, context);
                context.apply_reg_value(idx as u8, val);
            }
        }
        context.apply_reg_value(7, self.eval_cfa(context));
    }

    pub fn eval_return_address(&self, context: &CpuContext) -> u64 {
        self.eval_reg(self.return_addr_col as u8, context) as u64
    }
}

pub type UnwindExceptionCleanupFn = extern "C" fn(UnwindReasonCode, *mut UnwindException);

#[derive(Debug)]
#[repr(C)]
pub struct Exception {
    unwind_exception: UnwindException,
    payload: PanicPayload,
}

#[derive(Debug)]
#[repr(C, align(8))]
pub struct UnwindException {
    exception_class: u64,
    exception_cleanup: UnwindExceptionCleanupFn,
    private_1: u64,
    private_2: u64,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(C)]
pub enum UnwindReasonCode {
    NoReason = 0,
    ForeignExceptionCaught = 1,
    FatalPhase2Error = 2,
    FatalPhase1Error = 3,
    NormalStop = 4,
    EndOfStack = 5,
    HandlerFound = 6,
    InstallContext = 7,
    ContinueUnwind = 8,
}

#[derive(Debug)]
#[repr(C)]
pub struct UnwindContext {
    fde: Option<ParsedFde<'static>>,
    cpu_context: CpuContext,
    ip: u64,
    search_table: EhFrameSearchTable<'static>,
    stop_location: u64,
}

#[no_mangle]
pub extern "C" fn _Unwind_RaiseException(exception: *mut UnwindException) -> UnwindReasonCode {
    let header = unsafe { crate::elf::Elf64Header::new() };
    let eh_frame_hdr = header
        .find_eh_frame_hdr()
        .expect("Failed to find eh_frame_hdr");
    let search_table: EhFrameSearchTable<'static> = eh_frame_hdr
        .frame_search_table()
        .expect("Failed to find frame search table");

    let mut original_context = CpuContext::zero();
    unsafe { getcontext(&mut original_context) };

    let mut exception_context = Box::new(UnwindContext {
        fde: None,
        cpu_context: original_context.clone(),
        ip: 0,
        search_table,
        stop_location: 0,
    });

    let exception_context = Box::into_raw(exception_context);

    // Search phase
    let mut cursor = {
        let context = unsafe { &*exception_context };
        context.cpu_context.rip
    };
    let handler = loop {
        {
            let rsp = {
                let context = unsafe { &*exception_context };
                context.cpu_context.rsp
            };
            tracing::trace!("walk stack cursor: {:x?} - {:x}", cursor, rsp);
        }
        let fde = unsafe {
            let context = &*exception_context;
            context.search_table.lookup(cursor as usize)
        };
        let fde = match fde {
            None => return UnwindReasonCode::EndOfStack,
            Some(fde) => fde,
        };

        {
            let exception_context = unsafe { &mut *exception_context };
            exception_context.fde = Some(fde.clone());
            exception_context.ip = cursor;
        }

        if let Some(personality) = fde.personality() {
            let personality_result = unsafe {
                personality(
                    1,
                    UNWIND_ACTION_SEARCH_PHASE,
                    RUST_EXCEPTION_CLASS,
                    exception,
                    exception_context,
                )
            };
            match personality_result {
                UnwindReasonCode::HandlerFound => break cursor,
                UnwindReasonCode::ContinueUnwind => {}
                _ => panic!(
                    "Incorrect personality response in search phase: {:?}",
                    personality_result
                ),
            }
        }

        {
            let context = unsafe { &mut *exception_context };
            let p = Processed::process(cursor as usize, &fde);
            let new_cursor = p.eval_return_address(&context.cpu_context);
            p.apply_to(&mut context.cpu_context);
            assert_ne!(cursor, new_cursor);
            cursor = new_cursor;
        }
    };

    {
        let context = unsafe { &mut *exception_context };
        context.stop_location = handler;
        context.cpu_context = original_context.clone();
    }

    // Cleanup phase
    cleanup(exception, exception_context);

    todo!("_Unwind_RaiseException")
}

fn cleanup(exception_object: *mut UnwindException, exception_context: *mut UnwindContext) {
    let mut cursor = {
        let context = unsafe { &*exception_context };
        context.cpu_context.rip
    };
    let handler_addr = {
        let context = unsafe { &*exception_context };
        context.stop_location
    };
    loop {
        {
            let rsp = {
                let context = unsafe { &*exception_context };
                context.cpu_context.rsp
            };
            tracing::trace!("cleanup walk stack cursor: {:x?} - {:x}", cursor, rsp);
        }
        let fde = unsafe {
            let context = &*exception_context;
            context.search_table.lookup(cursor as usize)
        };
        let fde = match fde {
            None => todo!("End of Stack"),
            Some(fde) => fde,
        };

        {
            let exception_context = unsafe { &mut *exception_context };
            exception_context.fde = Some(fde.clone());
            exception_context.ip = cursor;
        }

        {
            let context = unsafe { &*exception_context };
            tracing::trace!("context: {:x?}", context.cpu_context);
        }
        if let Some(personality) = fde.personality() {
            let mut action = UNWIND_ACTION_CLEANUP_PHASE;
            if cursor == handler_addr {
                action |= UNWIND_ACTION_HANDLER_FRAME;
            }
            let personality_result = unsafe {
                personality(
                    1,
                    action,
                    RUST_EXCEPTION_CLASS,
                    exception_object,
                    exception_context,
                )
            };
            match personality_result {
                UnwindReasonCode::InstallContext => {
                    let context = unsafe { &*exception_context };
                    tracing::trace!("installing context: {:x?}", context.cpu_context);
                    unsafe { setcontext(&context.cpu_context) }
                }
                UnwindReasonCode::ContinueUnwind => {}
                _ => panic!("excepted InstallContext, got {:?}", personality_result),
            }
        }

        {
            let context = unsafe { &mut *exception_context };
            let p = Processed::process(cursor as usize, &fde);
            let new_cursor = p.eval_return_address(&context.cpu_context);
            p.apply_to(&mut context.cpu_context);
            assert_ne!(cursor, new_cursor);
            cursor = new_cursor;
        }
    }
}

pub unsafe fn cleanup_exception(ptr: *mut u8) -> PanicPayload {
    let exception = ptr as *mut UnwindException;
    if (*exception).exception_class != RUST_EXCEPTION_CLASS {
        _Unwind_DeleteExceltion(exception);
        panic!("Foreign exception");
    }
    let exception = Box::from_raw(exception as *mut Exception);
    exception.payload
}

pub type UnwindAction = c_int;

pub const UNWIND_ACTION_SEARCH_PHASE: UnwindAction = 1;
pub const UNWIND_ACTION_CLEANUP_PHASE: UnwindAction = 2;
pub const UNWIND_ACTION_HANDLER_FRAME: UnwindAction = 4;
pub const UNWIND_ACTION_FORCE_UNWIND: UnwindAction = 8;

pub type UnwindStopFn = extern "C" fn(
    c_int,
    UnwindAction,
    *const UnwindException,
    *const UnwindContext,
    *const c_void,
) -> UnwindReasonCode;

#[no_mangle]
pub extern "C" fn _Unwind_ForcedUnwind(
    exception: *const UnwindException,
    stop: UnwindStopFn,
    stop_param: *const c_void,
) -> UnwindReasonCode {
    todo!()
}

#[no_mangle]
pub extern "C" fn _Unwind_Resume(exception: *mut UnwindException) {
    let header = unsafe { crate::elf::Elf64Header::new() };
    let eh_frame_hdr = header
        .find_eh_frame_hdr()
        .expect("Failed to find eh_frame_hdr");
    let search_table: EhFrameSearchTable<'static> = eh_frame_hdr
        .frame_search_table()
        .expect("Failed to find frame search table");

    let mut original_context = CpuContext::zero();
    unsafe { getcontext(&mut original_context) };

    let mut exception_context = Box::new(UnwindContext {
        fde: None,
        cpu_context: original_context.clone(),
        ip: 0,
        search_table,
        stop_location: 0,
    });

    let exception_context = Box::into_raw(exception_context);

    cleanup(exception, exception_context);
    panic!("Should never get here");
}

#[no_mangle]
pub extern "C" fn _Unwind_DeleteExceltion(exception_ptr: *mut UnwindException) {
    let exception = unsafe { &*exception_ptr };
    (exception.exception_cleanup)(UnwindReasonCode::NoReason, exception_ptr);
}

#[no_mangle]
pub extern "C" fn _Unwind_GetGR(context_ptr: *const UnwindContext, index: c_int) -> u64 {
    let context = unsafe { &*context_ptr };
    context.cpu_context.get_reg_value(index as u8) as u64
}

#[no_mangle]
pub extern "C" fn _Unwind_SetGR(context_ptr: *mut UnwindContext, index: c_int, new: u64) {
    let context = unsafe { &mut *context_ptr };
    context
        .cpu_context
        .apply_reg_value(index as u8, new as u128);
}

#[no_mangle]
pub extern "C" fn _Unwind_GetIP(context_ptr: *const UnwindContext) -> u64 {
    let context = unsafe { &*context_ptr };
    context.cpu_context.rip
}

#[no_mangle]
pub extern "C" fn _Unwind_SetIP(context_ptr: *mut UnwindContext, new: u64) {
    let context = unsafe { &mut *context_ptr };
    context.cpu_context.rip = new;
}

#[no_mangle]
pub extern "C" fn _Unwind_GetLanguageSpecificData(context_ptr: *const UnwindContext) -> u64 {
    unsafe { &*context_ptr }
        .fde
        .as_ref()
        .expect("No FDE set")
        .lsda()
        .unwrap_or(0) as u64
}

#[no_mangle]
pub extern "C" fn _Unwind_GetRegionStart(context_ptr: *const UnwindContext) -> u64 {
    let context = unsafe { &*context_ptr };
    context.fde.as_ref().expect("No FDE set").initial_location() as u64
}

#[no_mangle]
pub extern "C" fn _Unwind_GetCFA(context_ptr: *const UnwindContext) -> u64 {
    todo!("_Unwind_GetCFA")
}

pub type PersonalityRoutine = unsafe extern "C" fn(
    c_int,
    UnwindAction,
    u64,
    *mut UnwindException,
    *mut UnwindContext,
) -> UnwindReasonCode;

const RUST_EXCEPTION_CLASS: u64 = 0x4d4f5a_00_52555354;

pub fn start_unwind(payload: PanicPayload) -> UnwindReasonCode {
    let exception = Box::new(Exception {
        unwind_exception: UnwindException {
            exception_class: RUST_EXCEPTION_CLASS,
            exception_cleanup,
            private_1: 0,
            private_2: 0,
        },
        payload,
    });
    let exception_param = Box::into_raw(exception);

    return _Unwind_RaiseException(exception_param as *mut UnwindException);

    extern "C" fn exception_cleanup(
        _unwind_code: UnwindReasonCode,
        exception: *mut UnwindException,
    ) {
        unsafe {
            let _: Box<Exception> = Box::from_raw(exception as *mut Exception);
            panic!("panic rethrown?")
        }
    }
}

#[lang = "eh_personality"]
pub unsafe extern "C" fn eh_personality(
    version: c_int,
    action: UnwindAction,
    exception_class: u64,
    exception_object: *mut UnwindException,
    context: *mut UnwindContext,
) -> UnwindReasonCode {
    if exception_class != RUST_EXCEPTION_CLASS {
        return UnwindReasonCode::ForeignExceptionCaught;
    }

    let region_start = _Unwind_GetRegionStart(context);

    let ip = _Unwind_GetIP(context) - 1;

    let lsda_addr = _Unwind_GetLanguageSpecificData(context);
    let lsda = ParsedLsdaEntry::from_addr(lsda_addr, region_start);
    tracing::trace!("lsda: {:x?}", lsda);
    let callsite = lsda.find_callsite(ip).expect("Failed to find callsite");
    tracing::trace!("callsite: {:x?}", callsite);

    if action & UNWIND_ACTION_SEARCH_PHASE != 0 {
        if callsite.action().is_none() {
            return UnwindReasonCode::ContinueUnwind;
        }
        return UnwindReasonCode::HandlerFound;
    } else if action & UNWIND_ACTION_CLEANUP_PHASE != 0 {
        if callsite.landing_pad().is_some() {
            let landing_pad = callsite.landing_pad().expect("Landingpad gone missing?");
            _Unwind_SetIP(context, landing_pad.get() as u64);
            _Unwind_SetGR(context, 0, exception_object as u64);
            _Unwind_SetGR(context, 1, 0);
            return UnwindReasonCode::InstallContext;
        }

        if callsite.action().is_some() {
            todo!("actions");
        }

        if action & UNWIND_ACTION_HANDLER_FRAME != 0 {
            todo!()
        } else {
            UnwindReasonCode::ContinueUnwind
        }
    } else {
        panic!("Unknown action: {}", action);
    }
}

#[cfg(test)]
mod tests {
    use crate::unwind::{eh_personality, PersonalityRoutine};

    #[test_case]
    fn eh_personality_is_personality_routine() {
        let _: PersonalityRoutine = eh_personality;
    }
}
