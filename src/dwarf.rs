use crate::unwind::PersonalityRoutine;
use byteorder::{ByteOrder, NativeEndian};
use core::cmp::Ordering;
use core::fmt;
use core::fmt::Formatter;
use core::num::{NonZeroU64, NonZeroUsize};
use core::ptr::{null, NonNull};
use nano_leb128::{SLEB128, ULEB128};

const DW_EH_PE_OMIT: u8 = 0xFF;

const DW_EH_PE_ULEB128: u8 = 0x01;
const DW_EH_PE_UDATA2: u8 = 0x02;
const DW_EH_PE_UDATA4: u8 = 0x03;
const DW_EH_PE_UDATA8: u8 = 0x04;
const DW_EH_PE_SLEB128: u8 = 0x09;
const DW_EH_PE_SDATA2: u8 = 0x0A;
const DW_EH_PE_SDATA4: u8 = 0x0B;
const DW_EH_PE_SDATA8: u8 = 0x0C;

const DW_EH_PE_ABSPTR: u8 = 0x00;
const DW_EH_PE_PCREL: u8 = 0x10;
const DW_EH_PE_DATAREL: u8 = 0x30;

const DW_EH_PE_INDIRECT: u8 = 0x80;

#[repr(C)]
pub struct EhFrameHeader {
    version: u8,
    eh_frame_ptr_enc: u8,
    fde_count_enc: u8,
    table_enc: u8,
    data: [u8],
}

impl fmt::Debug for EhFrameHeader {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Self>())
            .field("version", &self.version)
            .field("eh_frame_ptr_end", &self.eh_frame_ptr_enc)
            .field("fde_count_enc", &self.fde_count_enc)
            .field("table_enc", &self.table_enc)
            .finish()
    }
}

impl EhFrameHeader {
    /// Constructs the EhFrameHeader from a pointer and byte size.
    ///
    /// # Safety
    /// ptr must point to an instance of an EhFrameHeader
    pub unsafe fn from_raw_parts(ptr: *const u8, byte_len: usize) -> &'static Self {
        let slice = core::slice::from_raw_parts(ptr, byte_len);
        &*(slice as *const [u8] as *const Self)
    }

    pub fn frame_search_table(&self) -> Option<EhFrameSearchTable> {
        let self_addr = self as *const _ as *const u8 as usize;
        let data = &self.data;
        let (eh_frame_ptr, data) = parse_encoded(self_addr, self.eh_frame_ptr_enc, data)?;
        let (fde_count, data) = parse_encoded(self_addr, self.fde_count_enc, data)?;
        let entry_size = encoded_size(self.table_enc) * 2;
        let table_len = fde_count * entry_size;
        Some(EhFrameSearchTable {
            eh_frame_hdr: self,
            eh_frame_ptr: eh_frame_ptr as *const u8,
            fde_count,
            entry_size,
            table_data: &data[..table_len],
        })
    }
}

pub struct EhFrameSearchTable<'a> {
    eh_frame_hdr: &'a EhFrameHeader,
    eh_frame_ptr: *const u8,
    fde_count: usize,
    entry_size: usize,
    table_data: &'a [u8],
}

impl<'a> fmt::Debug for EhFrameSearchTable<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Self>())
            .field("eh_frame_ptr", &self.eh_frame_ptr)
            .field("fde_count", &self.fde_count)
            .field("entry_size", &self.entry_size)
            .finish()
    }
}

impl<'a> EhFrameSearchTable<'a> {
    fn lookup_maybe(&self, addr: usize) -> usize {
        assert!(self.fde_count > 1);

        let header_addr = self.eh_frame_hdr as *const _ as *const u8 as usize;

        let mut search_frame = self.table_data;
        let mut search_frame_count = self.fde_count;
        while search_frame_count > 1 {
            let (head, tail) = search_frame.split_at((search_frame_count / 2) * self.entry_size);
            let pivot_ip = parse_encoded(header_addr, self.eh_frame_hdr.table_enc, tail)
                .expect("Failed to read table entry")
                .0;
            match pivot_ip.cmp(&addr) {
                Ordering::Less => {
                    search_frame = tail;
                    search_frame_count = search_frame_count - (search_frame_count / 2);
                }
                Ordering::Equal => {
                    search_frame = tail;
                    break;
                }
                Ordering::Greater => {
                    search_frame = head;
                    search_frame_count /= 2;
                }
            }
        }

        search_frame = &search_frame[(self.entry_size / 2)..];

        parse_encoded(header_addr, self.eh_frame_hdr.table_enc, search_frame)
            .expect("Failed to parse fde pointer")
            .0
    }

    pub fn lookup(&self, addr: usize) -> Option<ParsedFde<'a>> {
        let fde_ptr = self.lookup_maybe(addr) as *const u8;
        let mut fde_len = unsafe { (fde_ptr as *const u32).read() } as usize;
        assert_ne!(0, fde_len);
        assert_ne!(0xFFFF_FFFF, fde_len, "64-bit dwarf object");

        fde_len -= core::mem::size_of::<u32>(); // TODO

        let fde_slice = unsafe { core::slice::from_raw_parts(fde_ptr, fde_len) };
        let fde = unsafe { &*(fde_slice as *const [u8] as *const Fde) }.parse();
        if fde.contains(addr) {
            Some(fde) // todo: check if addr is really in fde
        } else {
            None
        }
    }
}

#[repr(C)]
pub struct Fde {
    length: u32,
    cie_pointer: u32,
    data: [u8],
}

impl fmt::Debug for Fde {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Fde>())
            .field("length", &self.length)
            .field("cie_pointer", &self.cie_pointer)
            .finish()
    }
}

impl Fde {
    pub fn cie(&self) -> &Cie {
        let cie_ptr = unsafe {
            ((&self.cie_pointer) as *const _ as *const u8).offset(-(self.cie_pointer as isize))
        };
        let mut cie_len = unsafe { (cie_ptr as *const u32).read() } as usize;
        assert_ne!(0, cie_len);
        assert_ne!(0xFFFF_FFFF, cie_len, "64-bit dwarf object");

        // TODO: figure out nicer way to do this
        cie_len -= core::mem::size_of::<u32>();
        cie_len -= core::mem::size_of::<u8>();

        let cie_slice = unsafe { core::slice::from_raw_parts(cie_ptr, cie_len) };
        let cie = unsafe { &*(cie_slice as *const [u8] as *const Cie) };
        assert_eq!(0, cie.cie_id);
        cie
    }

    pub fn parse(&self) -> ParsedFde {
        let data = &self.data;
        let cie = self.cie().parse();

        let (address_start, data) =
            parse_encoded(0, cie.fde_enc, data).expect("Failed to read FDE start address");
        let (address_range, data) =
            parse_encoded(0, cie.fde_enc, data).expect("Failed to read FDE address range");

        let mut lsda = None;
        let mut data = data;
        if cie.augmentation.starts_with('z') {
            let (aug_data_len, offset) =
                ULEB128::read_from(data).expect("Failed to read aug data length");
            data = &data[offset..];
            for c in cie.augmentation.chars().skip(1) {
                match c {
                    'L' => {
                        let (lsda_data, rest) = parse_encoded(0, cie.lsda_enc, data)
                            .expect("Failed to read lsda pointer");
                        lsda = Some(lsda_data);
                        data = rest;
                    }
                    _ => {}
                }
            }
        }

        ParsedFde {
            cie,
            length: self.length,
            cie_pointer: self.cie_pointer,
            address_start,
            address_range,
            lsda,
            instructions: data,
        }
    }
}

#[derive(Clone)]
pub struct ParsedFde<'a> {
    cie: ParsedCie<'a>,
    length: u32,
    cie_pointer: u32,
    address_start: usize,
    address_range: usize,
    lsda: Option<usize>,
    instructions: &'a [u8],
}

impl<'a> fmt::Debug for ParsedFde<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Self>())
            .field("length", &self.length)
            .field("cie_pointer", &self.cie_pointer)
            .field("address_start", &self.address_start)
            .field("address_range", &self.address_range)
            .field("lsda", &self.lsda)
            .finish()
    }
}

impl<'a> ParsedFde<'a> {
    pub fn cie(&self) -> &ParsedCie<'a> {
        &self.cie
    }

    pub fn contains(&self, addr: usize) -> bool {
        self.address_start <= addr && addr < self.address_start + self.address_range
    }

    pub fn initial_location(&self) -> usize {
        self.address_start
    }

    pub fn instructions(&self) -> InstructionReader {
        InstructionReader::new(
            self.cie.code_alignment_factor,
            self.cie.data_alignment_factor,
            &self.instructions,
        )
    }

    pub fn lsda(&self) -> Option<usize> {
        self.lsda.as_ref().cloned()
    }

    pub fn personality(&self) -> Option<PersonalityRoutine> {
        self.cie.personality_routine.as_ref().map(|&addr| {
            let p = unsafe { core::mem::transmute(addr as *const u8) };
            p
        })
    }
}

#[repr(C)]
pub struct Cie {
    length: u32,
    cie_id: u32,
    version: u8,
    data: [u8],
}

impl fmt::Debug for Cie {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Cie>())
            .field("length", &self.length)
            .field("cie_id", &self.cie_id)
            .field("version", &self.version)
            .finish()
    }
}

impl Cie {
    pub fn parse(&self) -> ParsedCie {
        let aug_len = self.data.iter().position(|c| *c == 0).unwrap();
        let data = &self.data;
        let (aug_slice, data) = self.data.split_at(aug_len);
        let aug = unsafe { &*(aug_slice as *const [u8] as *const str) };
        let data = &data[1..]; // Skip '\0' byte

        let (code_alignment_factor, offset) =
            ULEB128::read_from(data).expect("Failed to read code_alignment_factor");
        let data = &data[offset..];

        let (data_alignment_factor, offset) =
            SLEB128::read_from(data).expect("Failed to read data_alignment_factor");
        let data = &data[offset..];

        let (return_address_register, offset) =
            ULEB128::read_from(data).expect("Failed to read return_address_register");
        let mut data = &data[offset..];

        let mut fde_enc = DW_EH_PE_ABSPTR;
        let mut lsda_enc = DW_EH_PE_ABSPTR;
        let mut personality_routine = None;

        if aug.starts_with('z') {
            let (aug_data_len, offset) =
                ULEB128::read_from(data).expect("Failed to read aug data length");
            data = &data[offset..];
            for c in aug.chars().skip(1) {
                match c {
                    'P' => {
                        let personality_enc = data[0];
                        let (personality, rest) = parse_encoded(0, personality_enc, &data[1..])
                            .expect("Failed to read personality_routine"); // TODO frame_addr
                        data = rest;
                        personality_routine = Some(personality);
                    }
                    'L' => {
                        lsda_enc = data[0];
                        data = &data[1..];
                    }
                    'R' => {
                        fde_enc = data[0];
                        data = &data[1..];
                    }
                    _ => {}
                }
            }
        }

        ParsedCie {
            length: self.length,
            cie_id: self.cie_id,
            version: self.version,
            augmentation: aug,
            code_alignment_factor: code_alignment_factor.into(),
            data_alignment_factor: data_alignment_factor.into(),
            return_address_register: return_address_register.into(),
            fde_enc,
            lsda_enc,
            personality_routine,
            initial_instructions: data,
        }
    }
}

#[derive(Clone)]
pub struct ParsedCie<'a> {
    length: u32,
    cie_id: u32,
    version: u8,
    augmentation: &'a str,
    code_alignment_factor: u64,
    data_alignment_factor: i64,
    return_address_register: u64,
    fde_enc: u8,
    lsda_enc: u8,
    personality_routine: Option<usize>,
    initial_instructions: &'a [u8],
}

impl<'a> fmt::Debug for ParsedCie<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Self>())
            .field("length", &self.length)
            .field("cie_id", &self.cie_id)
            .field("version", &self.version)
            .field("augmentation", &self.augmentation)
            .field("code_alignment_factor", &self.code_alignment_factor)
            .field("data_alignment_factor", &self.data_alignment_factor)
            .field("return_address_register", &self.return_address_register)
            .field("fde_enc", &self.fde_enc)
            .field("lsda_end", &self.lsda_enc)
            .field("personality_routine", &self.personality_routine)
            .finish()
    }
}

impl<'a> ParsedCie<'a> {
    pub fn code_alignment_factor(&self) -> usize {
        self.code_alignment_factor as usize
    }

    pub fn return_address_register(&self) -> usize {
        self.return_address_register as usize
    }

    pub fn instructions(&self) -> InstructionReader {
        InstructionReader::new(
            self.code_alignment_factor,
            self.data_alignment_factor,
            &self.initial_instructions,
        )
    }
}

#[derive(Debug)]
struct ByteReader<'a> {
    data: &'a [u8],
    current: usize,
}

impl<'a> ByteReader<'a> {
    fn read_u8(&mut self) -> Option<u8> {
        let val = self.data.get(self.current)?;
        self.current += 1;
        Some(*val)
    }

    fn read_u16(&mut self) -> Option<u16> {
        let val = NativeEndian::read_u16(&self.data[self.current..]);
        self.current += 2;
        Some(val)
    }

    fn read_u32(&mut self) -> Option<u32> {
        let val = NativeEndian::read_u32(&self.data[self.current..]);
        self.current += 4;
        Some(val)
    }

    fn read_uleb(&mut self) -> Option<u64> {
        if self.current >= self.data.len() {
            return None;
        }
        let (val, offset) =
            ULEB128::read_from(&self.data[self.current..]).expect("Failed to read unsigned leb128");
        self.current += offset;
        Some(u64::from(val))
    }

    fn read_sleb(&mut self) -> Option<i64> {
        if self.current >= self.data.len() {
            return None;
        }
        let (val, offset) =
            SLEB128::read_from(&self.data[self.current..]).expect("Failed to read unsigned leb128");
        self.current += offset;
        Some(i64::from(val))
    }
    fn read_expression(&mut self) -> Option<Expression> {
        todo!("read expression")
    }
}

#[derive(Debug)]
pub struct InstructionReader<'a> {
    reader: ByteReader<'a>,
    code_alignment_factor: u64,
    data_alignment_factor: i64,
}

impl<'a> InstructionReader<'a> {
    fn new(code_alignment_factor: u64, data_alignment_factor: i64, data: &'a [u8]) -> Self {
        InstructionReader {
            reader: ByteReader { data, current: 0 },
            code_alignment_factor,
            data_alignment_factor,
        }
    }
}

impl<'a> Iterator for InstructionReader<'a> {
    type Item = Instruction;

    fn next(&mut self) -> Option<Self::Item> {
        use Instruction::*;

        let opcode = self.reader.read_u8()?;
        let hi_bits = opcode >> 6;
        let lo_bits = opcode & 0x3F;

        let parsed = match (hi_bits, lo_bits) {
            (0x1, delta) => AdvanceLoc {
                delta: (delta as u64) * self.code_alignment_factor,
            },
            (0x2, register) => {
                let offset = self.reader.read_uleb().unwrap() as i64;
                Offset {
                    register,
                    offset: offset * self.data_alignment_factor,
                }
            }
            (0x3, register) => Restore { register },
            (0x0, 0) => Nop,
            (0x0, 0x01) => {
                let address = self.reader.read_u8().unwrap();
                todo!("Really u8?");
                SetLoc { address } // TODO: really u8?
            }
            (0x0, 0x02) => {
                let delta = self.reader.read_u8().unwrap() as u64;
                AdvanceLoc {
                    delta: delta * self.code_alignment_factor,
                }
            }
            (0x0, 0x03) => {
                let delta = self.reader.read_u16().unwrap() as u64;
                AdvanceLoc {
                    delta: delta * self.code_alignment_factor,
                }
            }
            (0x0, 0x04) => {
                let delta = self.reader.read_u32().unwrap() as u64;
                AdvanceLoc {
                    delta: delta * self.code_alignment_factor,
                }
            }
            (0x0, 0x05) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                let offset = self.reader.read_uleb().unwrap() as i64;
                Offset {
                    register,
                    offset: offset * self.data_alignment_factor,
                }
            }
            (0x0, 0x06) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                RestoreExtended { register }
            }
            (0x0, 0x07) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                Undefined { register }
            }
            (0x0, 0x08) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                SameValue { register }
            }
            (0x0, 0x09) => {
                let register_from = self.reader.read_uleb().unwrap() as u8;
                let register_to = self.reader.read_uleb().unwrap() as u8;
                Register {
                    register_from,
                    register_to,
                }
            }
            (0x0, 0x0A) => RememberState,
            (0x0, 0x0B) => RestoreState,
            (0x0, 0x0C) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                let offset = self.reader.read_uleb().unwrap() as i64;
                DefCfa { register, offset }
            }
            (0x0, 0x0D) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                DefCfaRegister { register }
            }
            (0x0, 0x0E) => {
                let offset = self.reader.read_uleb().unwrap() as i64;
                DefCfaOffset { offset }
            }
            (0x0, 0x0F) => {
                let expression = self.reader.read_expression().unwrap();
                DefCfaExpression { expression }
            }
            (0x0, 0x10) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                let expression = self.reader.read_expression().unwrap();
                Expression {
                    register,
                    expression,
                }
            }
            (0x0, 0x11) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                let offset = self.reader.read_sleb().unwrap() as i64;
                Offset {
                    register,
                    offset: offset * self.data_alignment_factor,
                }
            }
            (0x0, 0x12) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                let offset = self.reader.read_sleb().unwrap();
                DefCfa {
                    register,
                    offset: offset * self.data_alignment_factor,
                }
            }
            (0x0, 0x13) => {
                let offset = self.reader.read_sleb().unwrap() as i64;
                DefCfaOffset {
                    offset: offset * self.data_alignment_factor,
                }
            }
            (0x0, 0x14) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                let offset = self.reader.read_uleb().unwrap() as i64;
                ValOffset {
                    register,
                    offset: offset * self.data_alignment_factor,
                }
            }
            (0x0, 0x15) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                let offset = self.reader.read_sleb().unwrap() as i64;
                ValOffset {
                    register,
                    offset: offset * self.data_alignment_factor,
                }
            }
            (0x0, 0x16) => {
                let register = self.reader.read_uleb().unwrap() as u8;
                let expression = self.reader.read_expression().unwrap();
                ValExpression {
                    register,
                    expression,
                }
            }
            (0x0, code) if 0x1C <= code && code <= 0x3F => {
                panic!("User opcode not implemented: {:x}", code)
            }
            (_, _) => panic!("Unknown opcode {:x}", opcode),
        };

        Some(parsed)
    }
}

#[derive(Debug)]
pub enum Instruction {
    SetLoc {
        address: u8,
    },
    AdvanceLoc {
        delta: u64,
    },

    DefCfa {
        register: u8,
        offset: i64,
    },
    DefCfaRegister {
        register: u8,
    },
    DefCfaOffset {
        offset: i64,
    },
    DefCfaExpression {
        expression: Expression,
    },

    Undefined {
        register: u8,
    },
    SameValue {
        register: u8,
    },
    Offset {
        register: u8,
        offset: i64,
    },
    ValOffset {
        register: u8,
        offset: i64,
    },
    Register {
        register_from: u8,
        register_to: u8,
    },
    Expression {
        register: u8,
        expression: Expression,
    },
    ValExpression {
        register: u8,
        expression: Expression,
    },

    Restore {
        register: u8,
    },
    RestoreExtended {
        register: u8,
    },

    RememberState,
    RestoreState,

    Nop,
}

#[derive(Debug)]
pub enum Expression {}

#[derive(Debug)]
pub struct ParsedLsdaEntry {
    landing_pad_base: u64,
    type_table_enc: u8,
    type_table: Option<u64>,
    call_site_table_enc: u8,
    call_site_table: &'static [u8],
}

impl ParsedLsdaEntry {
    pub fn from_addr(addr: u64, region_start: u64) -> Self {
        let mut data_ptr = addr as *const u8;

        let landing_pad_enc = unsafe { data_ptr.read_volatile() };
        data_ptr = unsafe { data_ptr.offset(1) };

        let landing_pad_base = if landing_pad_enc != DW_EH_PE_OMIT {
            let landing_pad_size = encoded_size(landing_pad_enc);
            let landing_pad_slice =
                unsafe { core::slice::from_raw_parts(data_ptr, landing_pad_size) };
            let (landing_pad_base, data) = parse_encoded(0, landing_pad_enc, landing_pad_slice)
                .expect("Failed to read landing pad");
            data_ptr = data.as_ptr();
            landing_pad_base as u64
        } else {
            region_start
        };

        let type_table_enc = unsafe { data_ptr.read_volatile() };
        data_ptr = unsafe { data_ptr.offset(1) };

        let type_table = if type_table_enc != DW_EH_PE_OMIT {
            let type_table_size = encoded_size(DW_EH_PE_ABSPTR | DW_EH_PE_ULEB128);
            let type_table_slice =
                unsafe { core::slice::from_raw_parts(data_ptr, type_table_size) };
            let (type_table, data) =
                parse_encoded(0, DW_EH_PE_ABSPTR | DW_EH_PE_ULEB128, type_table_slice)
                    .expect("Failed to read type table");
            data_ptr = data.as_ptr();
            Some(type_table as u64)
        } else {
            None
        };

        let call_site_table_enc = unsafe { data_ptr.read_volatile() };
        data_ptr = unsafe { data_ptr.offset(1) };

        let call_site_table = {
            let size = 8; // TODO
            let slice = unsafe { core::slice::from_raw_parts(data_ptr, size) };
            let (call_site_table_len, data) =
                parse_encoded(0, DW_EH_PE_ABSPTR | DW_EH_PE_ULEB128, slice)
                    .expect("Failed to read call site table length");
            data_ptr = data.as_ptr();
            unsafe { core::slice::from_raw_parts(data_ptr, call_site_table_len) }
        };

        ParsedLsdaEntry {
            landing_pad_base,
            type_table_enc,
            type_table,
            call_site_table_enc,
            call_site_table,
        }
    }

    pub fn callsites(&self) -> CallSites {
        CallSites::new(
            self.call_site_table_enc,
            self.landing_pad_base,
            self.call_site_table,
        )
    }

    pub fn find_callsite(&self, addr: u64) -> Option<ParsedCallSite> {
        for callsite in self.callsites() {
            if callsite.callsite > addr {
                return None;
            }
            let callsite_end = callsite.callsite + callsite.callsite_len as u64;
            if callsite_end > addr {
                return Some(callsite);
            }
        }
        None
    }
}

#[derive(Debug)]
pub struct ParsedCallSite {
    callsite: u64,
    callsite_len: usize,
    landing_pad: Option<NonZeroUsize>,
    action: Option<NonZeroUsize>,
}

impl ParsedCallSite {
    pub fn landing_pad(&self) -> Option<NonZeroUsize> {
        self.landing_pad.as_ref().cloned()
    }

    pub fn action(&self) -> Option<NonZeroUsize> {
        self.action.as_ref().cloned()
    }
}

#[derive(Debug)]
pub struct CallSites<'a> {
    encoding: u8,
    landing_pad_base: u64,
    remaining_data: &'a [u8],
}

impl<'a> CallSites<'a> {
    fn new(encoding: u8, landing_pad_base: u64, remaining_data: &'a [u8]) -> Self {
        CallSites {
            encoding,
            landing_pad_base,
            remaining_data,
        }
    }
}

impl<'a> Iterator for CallSites<'a> {
    type Item = ParsedCallSite;

    fn next(&mut self) -> Option<Self::Item> {
        if self.remaining_data.is_empty() {
            return None;
        }

        let data = self.remaining_data;

        let (callsite_offset, data) =
            parse_encoded(0, self.encoding, data).expect("Failed to read callsite address");
        let (callsite_len, data) =
            parse_encoded(0, self.encoding, data).expect("Failed to read callsite length");
        let (landing_pad_offset, data) =
            parse_encoded(0, self.encoding, data).expect("Failed to read callsite address");
        let (action, data) = parse_encoded(0, DW_EH_PE_ABSPTR | DW_EH_PE_ULEB128, data)
            .expect("Failed to read callsite address");

        self.remaining_data = data;

        Some(ParsedCallSite {
            callsite: self.landing_pad_base + callsite_offset as u64,
            callsite_len,
            landing_pad: NonZeroUsize::new(landing_pad_offset)
                .map(|offset| NonZeroUsize::new(offset.get() + self.landing_pad_base as usize))
                .flatten(),
            action: NonZeroUsize::new(action),
        })
    }
}

fn encoded_size(encoding: u8) -> usize {
    match encoding & 0xF {
        0xF => 0,
        DW_EH_PE_UDATA2 | DW_EH_PE_SDATA2 => 2,
        DW_EH_PE_UDATA4 | DW_EH_PE_SDATA4 => 4,
        DW_EH_PE_UDATA8 | DW_EH_PE_SDATA8 => 8,
        DW_EH_PE_SLEB128 | DW_EH_PE_ULEB128 => 8,
        _ => panic!("Unknown value encoding"),
    }
}

fn parse_encoded(frame_addr: usize, mut encoding: u8, mut data: &[u8]) -> Option<(usize, &[u8])> {
    if encoding == 0xFF {
        return None;
    }

    let is_indirect = if (encoding & DW_EH_PE_INDIRECT) != 0 {
        encoding &= !DW_EH_PE_INDIRECT;
        true
    } else {
        false
    };

    let ref_addr = match encoding & 0xF0 {
        DW_EH_PE_ABSPTR => 0isize,
        DW_EH_PE_PCREL => data.as_ptr() as isize,
        DW_EH_PE_DATAREL => frame_addr as isize,
        r => panic!("Unknown pointer relative type: {:x}", r),
    };

    let (mut ptr_value, remaining_data) = match encoding & 0xF {
        DW_EH_PE_UDATA2 => {
            let val = NativeEndian::read_u16(&data[..2]);
            (ref_addr + val as isize, &data[2..])
        }
        DW_EH_PE_UDATA4 => {
            let val = NativeEndian::read_u32(&data[..4]);
            (ref_addr + val as isize, &data[4..])
        }
        DW_EH_PE_UDATA8 => {
            let val = NativeEndian::read_u64(&data[..8]);
            (ref_addr + val as isize, &data[8..])
        }
        DW_EH_PE_SDATA2 => {
            let val = NativeEndian::read_i16(&data[..2]);
            (ref_addr + val as isize, &data[2..])
        }
        DW_EH_PE_SDATA4 => {
            let val = NativeEndian::read_i32(&data[..4]);
            (ref_addr + val as isize, &data[4..])
        }
        DW_EH_PE_SDATA8 => {
            let val = NativeEndian::read_i64(&data[..8]);
            (ref_addr + val as isize, &data[8..])
        }
        DW_EH_PE_SLEB128 => {
            let (val, offset) = SLEB128::read_from(data).expect("Failed to read sleb128");
            (i64::from(val) as isize, &data[offset..])
        }
        DW_EH_PE_ULEB128 => {
            let (val, offset) = ULEB128::read_from(data).expect("Failed to read sleb128");
            (u64::from(val) as isize, &data[offset..])
        }
        _ => panic!("Unknown value encoding"),
    };

    if is_indirect {
        ptr_value = unsafe { (ptr_value as *const isize).read() };
    }

    Some((ptr_value as usize, remaining_data))
}

#[cfg(test)]
mod tests {
    use super::*;
    use core::ptr::NonNull;

    static TEST_DATA: &[u8] = &[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    static TEST_DATA_NEGATIVE: &[u8] = &[0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 9, 10, 11];
    static TEST_EH_FRAME_ADDR: usize = 100;

    #[test_case]
    fn parse_ptr_omit() {
        let enc = DW_EH_PE_OMIT;

        let result = parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA);

        assert_eq!(None, result);
    }

    #[test_case]
    fn parse_ptr_udata2() {
        let enc = DW_EH_PE_ABSPTR | DW_EH_PE_UDATA2;

        let (parsed, remaining) = parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA).unwrap();

        assert_eq!(0x201, parsed);
        assert_eq!(&TEST_DATA[2..], remaining);
    }

    #[test_case]
    fn parse_ptr_udata4() {
        let enc = DW_EH_PE_ABSPTR | DW_EH_PE_UDATA4;

        let (parsed, remaining) = parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA).unwrap();

        assert_eq!(0x04030201, parsed);
        assert_eq!(&TEST_DATA[4..], remaining);
    }

    #[test_case]
    fn parse_ptr_udata8() {
        let enc = DW_EH_PE_ABSPTR | DW_EH_PE_UDATA8;

        let (parsed, remaining) = parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA).unwrap();

        assert_eq!(0x807060504030201, parsed);
        assert_eq!(&TEST_DATA[8..], remaining);
    }

    #[test_case]
    fn parse_ptr_sdata2() {
        let enc = DW_EH_PE_ABSPTR | DW_EH_PE_SDATA2;

        let (parsed, remaining) = parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA).unwrap();

        assert_eq!(0x201, parsed);
        assert_eq!(&TEST_DATA[2..], remaining);
    }

    #[test_case]
    fn parse_ptr_sdata4() {
        let enc = DW_EH_PE_ABSPTR | DW_EH_PE_SDATA4;

        let (parsed, remaining) = parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA).unwrap();

        assert_eq!(0x04030201, parsed);
        assert_eq!(&TEST_DATA[4..], remaining);
    }

    #[test_case]
    fn parse_ptr_sdata8() {
        let enc = DW_EH_PE_ABSPTR | DW_EH_PE_SDATA8;

        let (parsed, remaining) = parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA).unwrap();

        assert_eq!(0x807060504030201, parsed);
        assert_eq!(&TEST_DATA[8..], remaining);
    }

    #[test_case]
    fn parse_ptr_pcrel_sdata2() {
        let enc = DW_EH_PE_PCREL | DW_EH_PE_SDATA2;

        let (parsed, remaining) =
            parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA_NEGATIVE).unwrap();

        let expected = TEST_DATA_NEGATIVE.as_ptr() as usize - 1;
        assert_eq!(expected, parsed);
        assert_eq!(&TEST_DATA_NEGATIVE[2..], remaining);
    }

    #[test_case]
    fn parse_ptr_pcrel_sdata4() {
        let enc = DW_EH_PE_PCREL | DW_EH_PE_SDATA4;

        let (parsed, remaining) =
            parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA_NEGATIVE).unwrap();

        let expected = TEST_DATA_NEGATIVE.as_ptr() as usize - 1;
        assert_eq!(expected, parsed);
        assert_eq!(&TEST_DATA_NEGATIVE[4..], remaining);
    }

    #[test_case]
    fn parse_ptr_pcrel_sdata8() {
        let enc = DW_EH_PE_PCREL | DW_EH_PE_SDATA8;

        let (parsed, remaining) =
            parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA_NEGATIVE).unwrap();

        let expected = TEST_DATA_NEGATIVE.as_ptr() as usize - 1;
        assert_eq!(expected, parsed);
        assert_eq!(&TEST_DATA_NEGATIVE[8..], remaining);
    }

    #[test_case]
    fn parse_ptr_datarel_sdata2() {
        let enc = DW_EH_PE_DATAREL | DW_EH_PE_SDATA2;

        let (parsed, remaining) =
            parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA_NEGATIVE).unwrap();

        let expected = TEST_EH_FRAME_ADDR - 1;
        assert_eq!(expected, parsed);
        assert_eq!(&TEST_DATA_NEGATIVE[2..], remaining);
    }

    #[test_case]
    fn parse_ptr_datarel_sdata4() {
        let enc = DW_EH_PE_DATAREL | DW_EH_PE_SDATA4;

        let (parsed, remaining) =
            parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA_NEGATIVE).unwrap();

        let expected = TEST_EH_FRAME_ADDR - 1;
        assert_eq!(expected, parsed);
        assert_eq!(&TEST_DATA_NEGATIVE[4..], remaining);
    }

    #[test_case]
    fn parse_ptr_datarel_sdata8() {
        let enc = DW_EH_PE_DATAREL | DW_EH_PE_SDATA8;

        let (parsed, remaining) =
            parse_encoded(TEST_EH_FRAME_ADDR, enc, TEST_DATA_NEGATIVE).unwrap();

        let expected = TEST_EH_FRAME_ADDR - 1;
        assert_eq!(expected, parsed);
        assert_eq!(&TEST_DATA_NEGATIVE[8..], remaining);
    }

    #[test_case]
    fn parse_ptr_indirect() {
        let val = 0x1234usize;
        let data = (&val) as *const _ as u64;
        let mut buf = [0u8; 8];
        NativeEndian::write_u64(&mut buf, data);

        let enc = DW_EH_PE_INDIRECT | DW_EH_PE_ABSPTR | DW_EH_PE_SDATA8;

        let (parsed, _) = parse_encoded(TEST_EH_FRAME_ADDR, enc, &buf).unwrap();

        assert_eq!(val as usize, parsed);
    }
}
