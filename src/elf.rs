//! Structures for reading information from the ELF64 file header and program headers.
//! [Link to a specification](https://www.uclibc.org/docs/elf-64-gen.pdf)
//!
//! # Safety
//! This module assumes that the ELF header is mapped into 0x200000 in virtual memory.
//! Additionally the program header table and (if present) the .eh_frame_hdr and .eh_frame sections
//! must be mapped to a location with the appropriate offset to the ELF header.

use crate::dwarf::EhFrameHeader;
use core::fmt;

pub type Elf64Addr = u64;
pub type Elf64Half = u16;
pub type Elf64Off = u64;
pub type Elf64Sword = i32;
pub type Elf64Sxword = i64;
pub type Elf64Word = u32;
pub type Elf64Lword = u64;
pub type Elf64Xword = u64;

pub const NIDENT: usize = 16;

const HEADER_ADDR: usize = 0x200000;
const ELF_MAGIC_NUMBER: &[u8] = b"\x7FELF";

#[derive(Debug)]
#[repr(C)]
pub struct Elf64Header {
    ident: [u8; NIDENT],
    r#type: Elf64Half,
    machine: Elf64Half,
    version: Elf64Word,
    entry: Elf64Addr,
    phoff: Elf64Off,
    shoff: Elf64Off,
    flags: Elf64Word,
    ehsize: Elf64Half,
    phentsize: Elf64Half,
    phnum: Elf64Half,
    shentsize: Elf64Half,
    shnum: Elf64Half,
    shstrndx: Elf64Half,
}

impl Elf64Header {
    pub unsafe fn new() -> &'static Self {
        let ptr = HEADER_ADDR as *const u8;

        let ident_slice = core::slice::from_raw_parts(ptr, 4);
        assert_eq!(
            ident_slice, ELF_MAGIC_NUMBER,
            "ELF header does not contain magic number"
        );

        let ptr = ptr as *const Self;
        &*ptr
    }

    pub fn program_headers(&self) -> &[Elf64ProgramHeader] {
        assert_eq!(
            self.phentsize as usize,
            core::mem::size_of::<Elf64ProgramHeader>()
        );
        let offset = self.phoff;
        let ptr = unsafe {
            (self as *const _ as *const u8).offset(offset as isize) as *const Elf64ProgramHeader
        };
        let num = self.phnum as usize;
        unsafe { core::slice::from_raw_parts(ptr, num) }
    }

    pub fn section_headers(&self) -> &[Elf64SectionHeader] {
        assert_eq!(
            self.shentsize as usize,
            core::mem::size_of::<Elf64SectionHeader>()
        );
        let offset = self.shoff;
        let ptr = unsafe {
            (self as *const _ as *const u8).offset(offset as isize) as *const Elf64SectionHeader
        };
        let num = self.shnum as usize;
        unsafe { core::slice::from_raw_parts(ptr, num) }
    }

    pub fn find_eh_frame_hdr(&self) -> Option<&EhFrameHeader> {
        for ph in self.program_headers() {
            if ph.r#type() == Elf64ProgramHeaderType::GnuEhFrame {
                let addr = ph.vaddr as *mut u8;
                let size = ph.memsz as usize;
                return Some(unsafe { EhFrameHeader::from_raw_parts(addr, size) });
            }
        }

        None
    }
}

const PROGRAM_HEADER_GNU_EH_FRAME: Elf64Word = 0x6474e550;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Elf64ProgramHeaderType {
    Unknown,
    GnuEhFrame,
}

#[derive(Debug)]
#[repr(C)]
pub struct Elf64ProgramHeader {
    r#type: Elf64Word,
    flags: Elf64Word,
    offset: Elf64Off,
    vaddr: Elf64Addr,
    paddr: Elf64Addr,
    filesz: Elf64Xword,
    memsz: Elf64Xword,
    align: Elf64Xword,
}

impl Elf64ProgramHeader {
    pub fn r#type(&self) -> Elf64ProgramHeaderType {
        match self.r#type {
            PROGRAM_HEADER_GNU_EH_FRAME => Elf64ProgramHeaderType::GnuEhFrame,
            _ => Elf64ProgramHeaderType::Unknown,
        }
    }
}

#[derive(Debug)]
#[repr(C)]
pub struct Elf64SectionHeader {
    sh_name: Elf64Word,
    sh_type: Elf64Word,
    sh_flags: Elf64Lword,
    sh_addr: Elf64Lword,
    sh_offset: Elf64Lword,
    sh_size: Elf64Lword,
    sh_link: Elf64Word,
    sh_info: Elf64Word,
    sh_addralign: Elf64Lword,
    sh_entsize: Elf64Lword,
}
