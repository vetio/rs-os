use alloc::vec::Vec;
use spin::Mutex;
use core::task::{Waker, Context, Poll};
use crate::error::Error;
use alloc::boxed::Box;
use core::ops::Add;
use core::fmt;
use core::future::Future;
use core::pin::Pin;
use crate::CONTEXT;

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Instant {
    us: u64,
}

impl Instant {
    pub fn from_us(us: u64) -> Self {
        Instant { us }
    }

    pub fn as_us(self) -> u64 {
        self.us
    }
}

impl Add<Duration> for Instant {
    type Output = Instant;

    fn add(self, rhs: Duration) -> Self::Output {
        Instant {
            us: self.us + rhs.0
        }
    }
}

/// Duration for timers.
#[derive(Debug)]
pub struct Duration(u64);

impl Duration {
    pub fn from_us(us: u64) -> Duration {
        Duration(us)
    }

    pub fn from_ms(ms: u64) -> Duration {
        Self::from_us(ms * 1000)
    }

    pub fn from_s(s: u64) -> Duration {
        Self::from_ms(s * 1000)
    }
}

pub trait InterruptClock: fmt::Debug + Send + Sync {
    fn now(&self) -> Instant;
    fn schedule_interrupt(&self, when: Instant, f: Box<dyn FnOnce() + Send + 'static>);
}

impl<T> InterruptClock for &T
where T: InterruptClock {
    fn now(&self) -> Instant {
        (*self).now()
    }

    fn schedule_interrupt(&self, when: Instant, f: Box<dyn FnOnce() + Send>) {
        (*self).schedule_interrupt(when, f);
    }
}

pub struct Delay {
    when: Instant,
}

impl Delay {
    pub fn new(duration: Duration) -> Self {
        Delay {
            when: CONTEXT.get().unwrap().clock.now() + duration,
        }
    }
}

impl Future for Delay {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let clock =  &CONTEXT.get().unwrap().clock;
        let now = clock.now();
        if now >= self.when {
            return Poll::Ready(());
        }
        let waker = cx.waker().clone();
        clock.schedule_interrupt(self.when, Box::new(move || {
            waker.wake()
        }));
        Poll::Pending
    }
}
