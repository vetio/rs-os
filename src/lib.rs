#![no_std]
#![cfg_attr(test, no_main)]
#![feature(drain_filter)]
#![feature(optin_builtin_traits)]
#![feature(core_intrinsics)]
#![feature(abi_x86_interrupt)]
#![feature(alloc_error_handler)]
#![feature(const_mut_refs)]
#![feature(lang_items)]
#![feature(min_const_generics)]
#![feature(ptr_as_uninit)]
#![feature(maybe_uninit_slice)]
#![feature(slice_ptr_get)]
#![feature(wake_trait)]
#![feature(custom_test_frameworks)]
#![feature(unwind_attributes)]
#![feature(global_asm)]
#![test_runner(crate::test_runner::test_runner)]
#![reexport_test_harness_main = "test_main"]
#![recursion_limit = "1024"]

extern crate alloc;
pub mod r#async;
pub mod collections;
pub mod context;
pub mod dwarf;
pub mod elf;
pub mod float;
pub mod gdt;
pub mod interrupts;
pub mod logging;
pub mod memory;
pub mod mmr;
pub mod native;
pub mod panic;
pub mod platform;
pub mod prelude;
pub mod proptest;
pub mod qemu;
pub mod unwind;
pub mod error;

#[macro_use]
pub mod serial;
#[macro_use]
pub mod vga;

pub static CONTEXT: Once<Context> = Once::new();
pub static FRAME_ALLOCATOR: Once<Locked<BootInfoFrameAllocator>> = Once::new();
pub static PAGE_ALLOCATOR: Once<Locked<PageAllocator<'static>>> = Once::new();

#[cfg(test)]
bootloader::entry_point!(test_kernel_main);
#[cfg(test)]
fn test_kernel_main(boot_info: &'static bootloader::BootInfo) -> ! {
    init(boot_info);
    tracing::subscriber::set_global_default(SerialLogger::new())
        .expect("Failed to set test logger");
    test_main();
    hlt_loop()
}

pub fn set_logger() {
    tracing::subscriber::set_global_default(SerialLogger::new()).expect("Failed to set logger");
    // log::set_logger(&logging::LOGGER).expect("Failed to set logger");
    // log::set_max_level(log::LevelFilter::max());
}

pub fn init(boot_info: &'static bootloader::BootInfo) {
    gdt::init();
    interrupts::init_idt();
    interrupts::init_interrupts();
    let frame_allocator =
        unsafe { memory::frame_allocator::BootInfoFrameAllocator::init(&boot_info.memory_map) };
    FRAME_ALLOCATOR.call_once(move || Locked::new(frame_allocator));
    let page_allocator = unsafe { PageAllocator::with_active_table(boot_info) };
    PAGE_ALLOCATOR.call_once(move || Locked::new(page_allocator));

    let acpi = unsafe { Acpi::new(boot_info.physical_memory_offset as usize) };
    let (lapic, io_apic) = unsafe { platform::apic::init(&acpi) };
    let clock = hpet::init(&acpi, io_apic).expect("Failed to init HPET");
    unsafe { pit::disable(); }
    // let clock = unsafe { pit::init(u16::MAX) };

    x86_64::instructions::interrupts::enable();
    let context = Context {
        memory_offset: boot_info.physical_memory_offset,
        lapic,
        io_apic,
        clock: alloc::sync::Arc::new(clock),
        acpi,
    };
    CONTEXT.call_once(move || context);
}

#[macro_export]
macro_rules! start {
    () => {
        rs_os::bootloader::entry_point!(kernel_start);
        fn kernel_start(boot_info: &'static bootloader::BootInfo) -> ! {
            rs_os::init(boot_info);
            rs_os::set_logger();
            main();
            rs_os::hlt_loop()
        }

        #[panic_handler]
        fn panic(info: &core::panic::PanicInfo) -> ! {
            rs_os::panic::panic(info)
        }
    };
}

use crate::context::Context;
use crate::logging::SerialLogger;
use crate::memory::allocators::locked::Locked;
use crate::memory::frame_allocator::BootInfoFrameAllocator;
use crate::memory::page_allocator::PageAllocator;
pub use bootloader;
use spin::Once;
use x86_64::instructions::hlt;
use crate::platform::acpi::Acpi;
use crate::platform::hpet::HPET;
use crate::platform::{hpet, pit};

pub mod test_runner;

pub fn hlt_loop() -> ! {
    loop {
        hlt()
    }
}
