use crate::hlt_loop;
use crate::prelude::*;
use crate::unwind::{UnwindException, UnwindReasonCode};
use alloc::boxed::Box;
use alloc::string::String;
use core::any::Any;
use core::panic::PanicInfo;
use core::sync::atomic::{AtomicUsize, Ordering};

static PANIC_COUNT: AtomicUsize = AtomicUsize::new(0);

#[derive(Debug)]
pub struct PanicPayload {
    msg: String,
}

impl PanicPayload {
    pub fn new(msg: String) -> Self {
        PanicPayload { msg }
    }

    pub fn msg(&self) -> &str {
        self.msg.as_str()
    }
}

pub fn panic(info: &PanicInfo) -> ! {
    let prev_panic_count = PANIC_COUNT.fetch_add(1, Ordering::SeqCst);
    if prev_panic_count > 0 {
        println!("Panic while panicking: {}", info);
        hlt_loop()
    }

    // let mut log = [0u8; 1024];
    // let len = usize::min(log.len(), crate::logging::LOGGER.content_len());
    // crate::logging::LOGGER.copy_end_to_slice(&mut log[..len]);
    // let log = core::str::from_utf8(&log[..len]).unwrap_or("unable to read log");
    // serial_println!("{}", log);
    let payload = PanicPayload::new(String::new());

    let reason_code = crate::unwind::start_unwind(payload);
    println!("Panic: {}", info);
    if reason_code != UnwindReasonCode::EndOfStack {
        println!("unwinding failed: {:?}", reason_code)
    }
    hlt_loop()
}

#[cfg_attr(test, panic_handler)]
pub fn test_panic(info: &PanicInfo) -> ! {
    use crate::qemu::{exit_qemu, QemuExitCode};
    serial_println!("[Failed]");
    serial_println!("Error: {}", info);
    exit_qemu(QemuExitCode::Failed)
}

pub auto trait UnwindSafe {}

pub fn catch_unwind<F: FnOnce() -> R + UnwindSafe, R>(f: F) -> Result<R, PanicPayload> {
    struct Data<F, R> {
        f: Option<F>,
        r: Option<Result<R, PanicPayload>>,
    }

    let mut data = Data {
        f: Some(f),
        r: None,
    };

    let try_result = unsafe {
        core::intrinsics::r#try(
            do_call::<F, R>,
            &mut data as *mut _ as *mut u8,
            do_catch::<F, R>,
        )
    };

    return data.r.take().expect("Result was not set");

    fn do_call<F: FnOnce() -> R + UnwindSafe, R>(data: *mut u8) {
        let data = data as *mut Data<F, R>;
        let data = unsafe { &mut *data };
        let f = data.f.take().unwrap();
        let r = f();
        data.r = Some(Ok(r));
    }

    fn do_catch<F: FnOnce() -> R + UnwindSafe, R>(data: *mut u8, payload: *mut u8) {
        let payload = unsafe { crate::unwind::cleanup_exception(payload) };

        let data = data as *mut Data<F, R>;
        let data = unsafe { &mut *data };
        data.r = Some(Err(payload));
    }
}

// #[allow(improper_ctypes)]
// extern "C" {
//     fn __rust_panic_cleanup(payload: *mut u8) -> *mut (dyn Any + Send + 'static);
//
//     /// `payload` is actually a `*mut &mut dyn BoxMeUp` but that would cause FFI warnings.
//     /// It cannot be `Box<dyn BoxMeUp>` because the other end of this call does not depend
//     /// on liballoc, and thus cannot use `Box`.
//     #[unwind(allowed)]
//     fn __rust_start_panic(payload: usize) -> u32;
// }
