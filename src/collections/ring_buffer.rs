use core::fmt;

#[derive(Debug)]
pub struct RingBuffer<const SIZE: usize> {
    start: usize,
    current: usize,
    buffer: [u8; SIZE],
}

impl<const SIZE: usize> RingBuffer<SIZE> {
    #[inline(always)]
    pub const fn new() -> Self {
        RingBuffer {
            start: 0,
            current: 0,
            buffer: [0; SIZE],
        }
    }

    pub fn len(&self) -> usize {
        if self.current < self.start {
            debug_assert!(self.start < SIZE);
            SIZE - self.start + self.current
        } else {
            self.current - self.start
        }
    }

    pub fn copy_end_to_slice(&self, mut target: &mut [u8]) {
        if target.len() > self.len() {
            panic!(
                "Can't read {} bytes from {} byte buffer",
                target.len(),
                self.len()
            );
        }
        let skip_len = self.len() - target.len();

        let mut start = self.start + skip_len;
        if self.current < start {
            debug_assert!(start < SIZE);
            let part_len = SIZE - start;
            target[..part_len].copy_from_slice(&self.buffer[start..]);
            target = &mut target[part_len..];
            start = 0;
        }
        target.copy_from_slice(&self.buffer[start..self.current]);
    }

    fn advance(&mut self, num: usize) {
        let mut new_pos = self.current + num;
        if new_pos >= SIZE {
            new_pos -= SIZE;
        }
        if (self.current < self.start || self.current > new_pos) && new_pos >= self.start {
            self.start = new_pos + 1;
        }
        self.current = new_pos;
    }

    pub fn clear(&mut self) {
        self.start = self.current;
    }
}

impl<const SIZE: usize> fmt::Write for RingBuffer<SIZE> {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let mut b = s.as_bytes();
        if b.len() >= SIZE {
            return Err(fmt::Error);
        }

        let pos = self.current;
        let end = pos + b.len();

        debug_assert!(pos < SIZE);

        if end >= SIZE {
            let part_len = SIZE - pos;
            self.buffer[pos..SIZE].copy_from_slice(&b[..part_len]);
            b = &b[part_len..];
            self.advance(part_len);
        }

        let pos = self.current;
        let end = pos + b.len();

        self.buffer[pos..end].copy_from_slice(b);
        self.advance(b.len());

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use core::fmt::Write;

    #[test_case]
    fn test_ring_buffer_input_reads_back() {
        let mut buffer = RingBuffer::<11>::new();

        let input = "test input";
        buffer.write_str(input).expect("Writing test input failed");

        let mut output = [0u8; 10];
        buffer.copy_end_to_slice(&mut output[..]);
        let read = core::str::from_utf8(&output[..]).expect("Reading text back as str failed");
        assert_eq!(input, read);
    }

    #[test_case]
    fn test_ring_buffer_input_reads_back_end() {
        let mut buffer = RingBuffer::<11>::new();

        let input = "test input";
        buffer.write_str(input).expect("Writing test input failed");

        let mut output = [0u8; 5];
        buffer.copy_end_to_slice(&mut output[..]);
        let read = core::str::from_utf8(&output[..]).expect("Reading text back as str failed");
        assert_eq!("input", read);
    }

    #[test_case]
    fn test_ring_buffer_writing_past_end_circles_back() {
        let mut buffer = RingBuffer::<11>::new();

        let input = "test input";
        buffer.write_str(input).expect("Writing test input failed");

        let input = "new";
        buffer.write_str(input).expect("Writing past end failed");

        let mut output = [0u8; 10];
        buffer.copy_end_to_slice(&mut output[..]);
        let read = core::str::from_utf8(&output[..]).expect("Reading text back as str failed");
        assert_eq!("t inputnew", read);
    }

    #[test_case]
    fn test_ring_buffer_input_too_long_fails() {
        let mut buffer = RingBuffer::<2>::new();
        buffer
            .write_str("12")
            .expect_err("Writing input that is equal to buffer size did not fail");
        buffer
            .write_str("123")
            .expect_err("Writing input that is longer than buffer size did not fail");
    }
}
