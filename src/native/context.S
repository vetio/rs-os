.set o_rax, 0
.set o_rbx, 8
.set o_rcx, 16
.set o_rdx, 24
.set o_rsi, 32
.set o_rdi, 40
.set o_rsp, 48
.set o_rbp, 56
.set o_r8, 64
.set o_r9, 72
.set o_r10, 80
.set o_r11, 88
.set o_r12, 96
.set o_r13, 104
.set o_r14, 112
.set o_r15, 120
.set o_rflags, 128
.set o_rip, 136
.set o_xmm0, 144
.set o_xmm1, 160
.set o_xmm2, 176
.set o_xmm3, 192
.set o_xmm4, 208
.set o_xmm5, 224
.set o_xmm6, 240
.set o_xmm7, 256
.set o_xmm8, 272
.set o_xmm9, 288
.set o_xmm10, 304
.set o_xmm11, 320
.set o_xmm12, 336
.set o_xmm13, 352
.set o_xmm14, 368
.set o_xmm15, 384

.text
.global getcontext
getcontext:
    movq %rax, o_rax(%rdi)
    movq %rbx, o_rbx(%rdi)
    movq %rcx, o_rcx(%rdi)
    movq %rdx, o_rdx(%rdi)
    movq %rsi, o_rsi(%rdi)
    movq %rdi, o_rdi(%rdi)
    movq %rbp, o_rbp(%rdi)
    movq %r8, o_r8(%rdi)
    movq %r9, o_r9(%rdi)
    movq %r10, o_r10(%rdi)
    movq %r11, o_r11(%rdi)
    movq %r12, o_r12(%rdi)
    movq %r13, o_r13(%rdi)
    movq %r14, o_r14(%rdi)
    movq %r15, o_r15(%rdi)
    movq %xmm0, o_xmm0(%rdi)
    movq %xmm1, o_xmm1(%rdi)
    movq %xmm2, o_xmm2(%rdi)
    movq %xmm3, o_xmm3(%rdi)
    movq %xmm4, o_xmm4(%rdi)
    movq %xmm5, o_xmm5(%rdi)
    movq %xmm6, o_xmm6(%rdi)
    movq %xmm7, o_xmm7(%rdi)
    movq %xmm8, o_xmm8(%rdi)
    movq %xmm9, o_xmm9(%rdi)
    movq %xmm10, o_xmm10(%rdi)
    movq %xmm11, o_xmm11(%rdi)
    movq %xmm12, o_xmm12(%rdi)
    movq %xmm13, o_xmm13(%rdi)
    movq %xmm14, o_xmm14(%rdi)
    movq %xmm15, o_xmm15(%rdi)

    movq (%rsp), %rcx
    movq %rcx, o_rip(%rdi)

    leaq 8(%rsp), %rcx
    movq %rcx, o_rsp(%rdi)
    ret

.global setcontext
setcontext:
    movq o_rax(%rdi), %rax
    movq o_rbx(%rdi), %rbx
    movq o_rcx(%rdi), %rcx
    movq o_rdx(%rdi), %rdx
    movq o_rsi(%rdi), %rsi
    movq o_rbp(%rdi), %rbp
    movq o_r8(%rdi), %r8
    movq o_r9(%rdi), %r9
    movq o_r10(%rdi), %r10
    movq o_r11(%rdi), %r11
    movq o_r12(%rdi), %r12
    movq o_r13(%rdi), %r13
    movq o_r14(%rdi), %r14
    movq o_r15(%rdi), %r15
    movq o_rsp(%rdi), %rsp
    movq o_xmm0(%rdi), %xmm0
    movq o_xmm1(%rdi), %xmm1
    movq o_xmm2(%rdi), %xmm2
    movq o_xmm3(%rdi), %xmm3
    movq o_xmm4(%rdi), %xmm4
    movq o_xmm5(%rdi), %xmm5
    movq o_xmm6(%rdi), %xmm6
    movq o_xmm7(%rdi), %xmm7
    movq o_xmm8(%rdi), %xmm8
    movq o_xmm9(%rdi), %xmm9
    movq o_xmm10(%rdi), %xmm10
    movq o_xmm11(%rdi), %xmm11
    movq o_xmm12(%rdi), %xmm12
    movq o_xmm13(%rdi), %xmm13
    movq o_xmm14(%rdi), %xmm14
    movq o_xmm15(%rdi), %xmm15

    movq o_rip(%rdi), %rcx
    push %rcx

    movq o_rcx(%rdi), %rcx
    movq o_rdi(%rdi), %rdi

    ret
