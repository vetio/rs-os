global_asm!(include_str!("context.S"));

macro_rules! make_context_struct {
    ($($reg:ident : $ty:ident,)*) => {
        #[derive(Debug, Clone)]
        #[repr(C)]
        pub struct CpuContext {
            $(pub $reg : $ty,)*
        }

        impl CpuContext {
            pub fn zero() -> Self {
                CpuContext {
                    $($reg: 0,)*
                }
            }
        }
    };
}

macro_rules! make_context_offsets {
    ($($reg:ident : $ty:ident,)*) => {
        struct ContextOffsets;
        impl ContextOffsets {
            fn register_list() -> &'static [&'static str] {
                &[$(core::stringify!($reg),)*]
            }

            fn for_all(f: impl Fn(&'static str, isize)) {
                let context = CpuContext::zero();
                let context_ptr = (&context) as *const _ as *const u8;
                $(
                    let field = &context.$reg;
                    let field_ptr = field as *const _ as *const u8;
                    let offset = unsafe { field_ptr.offset_from(context_ptr) };
                    f(core::stringify!($reg), offset);
                )*
            }
        }
    };
}

macro_rules! make_context {
    ($($reg:ident : $ty:ident,)*) => {
        make_context_struct!($($reg : $ty,)*);
        make_context_offsets!($($reg : $ty,)*);
    }
}

impl CpuContext {
    pub fn get_reg_value(&self, idx: u8) -> u128 {
        match idx {
            0 => self.rax as u128,
            1 => self.rdx as u128,
            2 => self.rcx as u128,
            3 => self.rbx as u128,
            4 => self.rsi as u128,
            5 => self.rdi as u128,
            6 => self.rbp as u128,
            7 => self.rsp as u128,
            8 => self.r8 as u128,
            9 => self.r9 as u128,
            10 => self.r10 as u128,
            11 => self.r11 as u128,
            12 => self.r12 as u128,
            13 => self.r13 as u128,
            14 => self.r14 as u128,
            15 => self.r15 as u128,
            16 => panic!("RA is not supposed to read from CpuContext"),
            17 => self.xmm0,
            18 => self.xmm1,
            19 => self.xmm2,
            20 => self.xmm3,
            21 => self.xmm4,
            22 => self.xmm5,
            23 => self.xmm6,
            24 => self.xmm7,
            25 => self.xmm8,
            26 => self.xmm9,
            27 => self.xmm10,
            28 => self.xmm11,
            29 => self.xmm12,
            30 => self.xmm13,
            31 => self.xmm14,
            32 => self.xmm15,
            _ => panic!("Unknown register: {}", idx),
        }
    }

    pub fn apply_reg_value(&mut self, idx: u8, val: u128) {
        match idx {
            0 => self.rax = val as u64,
            1 => self.rdx = val as u64,
            2 => self.rcx = val as u64,
            3 => self.rbx = val as u64,
            4 => self.rsi = val as u64,
            5 => self.rdi = val as u64,
            6 => self.rbp = val as u64,
            7 => self.rsp = val as u64,
            8 => self.r8 = val as u64,
            9 => self.r9 = val as u64,
            10 => self.r10 = val as u64,
            11 => self.r11 = val as u64,
            12 => self.r12 = val as u64,
            13 => self.r13 = val as u64,
            14 => self.r14 = val as u64,
            15 => self.r15 = val as u64,
            16 => self.rip = val as u64,
            17 => self.xmm0 = val,
            18 => self.xmm1 = val,
            19 => self.xmm2 = val,
            20 => self.xmm3 = val,
            21 => self.xmm4 = val,
            22 => self.xmm5 = val,
            23 => self.xmm6 = val,
            24 => self.xmm7 = val,
            25 => self.xmm8 = val,
            26 => self.xmm9 = val,
            27 => self.xmm10 = val,
            28 => self.xmm11 = val,
            29 => self.xmm12 = val,
            30 => self.xmm13 = val,
            31 => self.xmm14 = val,
            32 => self.xmm15 = val,
            _ => panic!("Unknown register: {}", idx),
        }
    }
}

make_context!(
    rax: u64,
    rbx: u64,
    rcx: u64,
    rdx: u64,
    rsi: u64,
    rdi: u64,
    rsp: u64,
    rbp: u64,
    r8: u64,
    r9: u64,
    r10: u64,
    r11: u64,
    r12: u64,
    r13: u64,
    r14: u64,
    r15: u64,
    rflags: u64,
    rip: u64,
    xmm0: u128,
    xmm1: u128,
    xmm2: u128,
    xmm3: u128,
    xmm4: u128,
    xmm5: u128,
    xmm6: u128,
    xmm7: u128,
    xmm8: u128,
    xmm9: u128,
    xmm10: u128,
    xmm11: u128,
    xmm12: u128,
    xmm13: u128,
    xmm14: u128,
    xmm15: u128,
);

extern "C" {
    pub fn getcontext(context: &mut CpuContext);
    pub fn setcontext(context: &CpuContext) -> !;
}

#[cfg(test)]
mod tests {
    use crate::native::context::{getcontext, ContextOffsets, CpuContext};

    #[test_case]
    fn context_offsets() {
        static DEFINE_PREFIX_OFFSET: &str = ".set o_";

        let context_header = include_str!("context.S");
        let registers = ContextOffsets::register_list();
        let mut parsed_registers = alloc::vec![0; registers.len()];

        for line in context_header.lines() {
            if line.starts_with(DEFINE_PREFIX_OFFSET) {
                let mut rest = line[DEFINE_PREFIX_OFFSET.len()..].split_ascii_whitespace();
                let reg = rest.next().expect("Failed to parse context.h");
                let reg = &reg[..(reg.len() - 1)]; // skip comma
                let reg_idx = match registers.iter().position(|r| *r == reg) {
                    None => continue,
                    Some(idx) => idx,
                };
                let offset: isize = rest
                    .next()
                    .expect("Failed to parse context.h")
                    .parse()
                    .expect("Failed to parse offset");
                parsed_registers[reg_idx] = offset;
            }
        }

        ContextOffsets::for_all(|reg, offset| {
            let reg_idx = registers.iter().position(|r| *r == reg).unwrap();
            let parsed_offset = parsed_registers[reg_idx];
            assert_eq!(offset, parsed_offset, "reg = {}", reg);
        });
    }

    #[test_case]
    fn getcontext_rax() {
        fn test(x: u64) -> u64 {
            x * 2
        }
        let mut context = CpuContext::zero();
        let _ = test(21);
        unsafe { getcontext(&mut context) };
        assert_eq!(42, context.rax);
    }
}
