use crate::r#async::timer::InterruptClock;
use alloc::sync::Arc;
use crate::platform::acpi::Acpi;
use crate::platform::apic::{LocalApic, IOApic};

pub struct Context {
    pub memory_offset: u64,
    pub acpi: Acpi,
    pub lapic: &'static LocalApic,
    pub io_apic: &'static IOApic,
    pub clock: Arc<dyn InterruptClock>,
}
