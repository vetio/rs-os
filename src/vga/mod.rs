use lazy_static::lazy_static;
use spin::Mutex;

const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;
pub const VGA_TEXT_BUFFER: usize = 0xb8000;

mod data;
mod writer;

use core::fmt::{Arguments, Write};
pub use data::{Color, ColorCode};
pub use writer::Writer;
use x86_64::instructions::interrupts::without_interrupts;

lazy_static! {
    static ref WRITER: Mutex<Writer<'static>> =
        Mutex::new(unsafe { Writer::new(VGA_TEXT_BUFFER as *mut ()) });
}

pub fn print(args: Arguments) {
    without_interrupts(|| {
        WRITER.lock().write_fmt(args).expect("print failed");
    });
}

#[macro_export]
macro_rules! print {
    ($($args:tt)*) => ($crate::vga::print(format_args!($($args)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($fmt:expr) => ($crate::print!(concat!($fmt, "\n")));
    ($fmt:expr, $($args:tt)*) => {
        $crate::print!(concat!($fmt, "\n"), $($args)*);
    };
}

// pub fn print(args: core::fmt::Arguments) {
//
// }
//
// #[macro_export]
// macro_rules! print {
//     ($($args:tt)*) => $crate::vga::print(format_args!($($args)*));
//
// }
