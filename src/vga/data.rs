use crate::vga::{BUFFER_HEIGHT, BUFFER_WIDTH};
use volatile::Volatile;

#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

#[derive(Default, Debug, Clone, Copy, Eq, PartialEq)]
pub struct ColorCode(u8);

impl ColorCode {
    pub fn new(foreground: Color, background: Color) -> Self {
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}

#[derive(Default, Debug, Clone, Copy, Eq, PartialEq)]
#[repr(C)]
pub struct ScreenChar {
    pub ascii: u8,
    pub color_code: ColorCode,
}

pub type ScreenCharBuffer = [[ScreenChar; super::BUFFER_WIDTH]; super::BUFFER_HEIGHT];

#[derive(Debug)]
#[repr(transparent)]
pub struct Buffer {
    chars: ScreenCharBuffer,
}

impl Buffer {
    pub fn chars_mut(&mut self) -> Volatile<&mut ScreenCharBuffer> {
        Volatile::new(&mut self.chars)
    }
}

impl Default for Buffer {
    fn default() -> Self {
        Buffer {
            chars: [[ScreenChar::default(); BUFFER_WIDTH]; BUFFER_HEIGHT],
        }
    }
}
