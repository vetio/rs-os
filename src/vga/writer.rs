use crate::vga::data::{Buffer, ScreenChar};
use crate::vga::{Color, ColorCode, BUFFER_HEIGHT, BUFFER_WIDTH};
use core::fmt;

pub struct Writer<'a> {
    column_position: usize,
    color_code: ColorCode,
    buffer: &'a mut Buffer,
}

impl Writer<'static> {
    /// Creates a new writer with
    ///
    /// # Safety
    /// - buffer_location must point to a 80x25 vga text buffer.
    /// - Only one writer must exist for any buffer.
    pub unsafe fn new(buffer_location: *mut ()) -> Self {
        let buffer = &mut *(buffer_location as *mut Buffer);
        Self::with_buffer(buffer)
    }
}

impl<'a> Writer<'a> {
    pub fn with_buffer(buffer: &'a mut Buffer) -> Self {
        Writer {
            column_position: 0,
            color_code: ColorCode::new(Color::LightCyan, Color::Black),
            buffer,
        }
    }

    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            byte => {
                if self.column_position >= BUFFER_WIDTH {
                    self.new_line();
                }

                let row = BUFFER_HEIGHT - 1;
                let col = self.column_position;

                let color_code = self.color_code;
                self.buffer
                    .chars_mut()
                    .as_mut_slice()
                    .index_mut(row)
                    .as_mut_slice()
                    .index_mut(col)
                    .write(ScreenChar {
                        ascii: byte,
                        color_code,
                    });
                self.column_position += 1;
            }
        }
    }

    fn new_line(&mut self) {
        let mut buffer = self.buffer.chars_mut();
        buffer.as_mut_slice().copy_within(1..BUFFER_HEIGHT, 0);
        for col in 0..BUFFER_WIDTH {
            buffer
                .as_mut_slice()
                .index_mut(BUFFER_HEIGHT - 1)
                .as_mut_slice()
                .index_mut(col)
                .update(|c| c.ascii = 0);
        }
        self.column_position = 0;
    }

    pub fn write_string(&mut self, text: &str) {
        for byte in text.bytes() {
            self.write_byte(byte);
        }
    }
}

impl<'a> fmt::Write for Writer<'a> {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_string(s);
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use core::fmt::Write;

    #[test_case]
    fn test_writer_single_line() {
        let test_string = "fits into single line";
        let mut buffer = Buffer::default();
        let mut writer = Writer::with_buffer(&mut buffer);

        writer.write_string(test_string);

        drop(writer);
        for (col, c) in test_string.bytes().enumerate() {
            assert_eq!(
                c,
                buffer.chars_mut().extract_inner()[BUFFER_HEIGHT - 1][col].ascii
            );
        }
    }

    #[test_case]
    fn test_writer_multi_line() {
        let test_strings = ["First line", "2nd line"];
        let mut buffer = Buffer::default();
        let mut writer = Writer::with_buffer(&mut buffer);

        for s in test_strings.iter() {
            writer.write_string(s);
            writer.write_byte(b'\n');
        }

        drop(writer);
        let row_offset = BUFFER_HEIGHT - test_strings.len() - 1;
        for (row, s) in test_strings.iter().enumerate() {
            for (col, c) in s.bytes().enumerate() {
                assert_eq!(
                    c,
                    buffer.chars_mut().extract_inner()[row_offset + row][col].ascii,
                    "s = {:?}, row = {}, col = {}",
                    s,
                    row_offset + row,
                    col
                );
            }
        }
    }

    #[test_case]
    fn test_writer_new_line_clears_last_row() {
        let test_string = "some text\n";
        let mut buffer = Buffer::default();
        let mut writer = Writer::with_buffer(&mut buffer);

        writer.write_string(test_string);

        drop(writer);
        for c in buffer.chars_mut().extract_inner()[BUFFER_HEIGHT - 1].iter() {
            assert_eq!(0, c.ascii);
        }
    }

    #[test_case]
    fn test_writer_fmt() {
        let mut buffer = Buffer::default();
        let mut writer = Writer::with_buffer(&mut buffer);

        write!(writer, "formatted: {} - {}", 42, 1.234f32).expect("write failed");
        let expected = "formatted: 42 - 1.234";

        drop(writer);
        for (col, c) in expected.bytes().enumerate() {
            assert_eq!(
                c,
                buffer.chars_mut().extract_inner()[BUFFER_HEIGHT - 1][col].ascii
            );
        }
    }
}
