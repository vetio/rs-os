use crate::qemu::{exit_qemu, QemuExitCode};
use core::panic::PanicInfo;

pub trait Testable {
    fn run(&self);
}

impl<T: Fn()> Testable for T {
    fn run(&self) {
        serial_print!("{}...", core::any::type_name::<T>());
        self();
        serial_println!("[Ok]");
    }
}

pub fn test_runner(tests: &[&dyn Testable]) {
    for test in tests {
        test.run();
    }
    exit_qemu(QemuExitCode::Success);
}

pub fn test_panic(info: &PanicInfo) -> ! {
    serial_println!("[Failed]");
    serial_println!("Error: {}", info);
    exit_qemu(QemuExitCode::Failed)
}

#[macro_export]
macro_rules! test_start {
    () => {
        use core::panic::PanicInfo;
        use rs_os::hlt_loop;

        rs_os::bootloader::entry_point!(kernel_start);
        fn kernel_start(boot_info: &'static rs_os::bootloader::BootInfo) -> ! {
            rs_os::set_logger();
            rs_os::init(boot_info);
            test_main();
            hlt_loop()
        }

        #[panic_handler]
        fn panic(info: &PanicInfo) -> ! {
            rs_os::test_runner::test_panic(info)
        }
    };
}
