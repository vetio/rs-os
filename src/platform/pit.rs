use x86_64::instructions::port::Port;
use crate::r#async::timer::{InterruptClock, Instant};
use core::task::Waker;
use core::sync::atomic::{AtomicU16, AtomicU64, Ordering};
use spin::Mutex;
use pc_keyboard::KeyCode::Mute;
use x86_64::instructions::interrupts::without_interrupts;
use alloc::vec::Vec;
use alloc::boxed::Box;
use core::fmt;

const CHANNEL_0_DATA_PORT: u16 = 0x40;
const CHANNEL_1_DATA_PORT: u16 = 0x41;
const CHANNEL_2_DATA_PORT: u16 = 0x42;
const MODE_COMMAND_REGISTER: u16 = 0x43;

const TIMER_FREQ_HZ: u64 = 1193182;

static TIMER: Pit = Pit::new();

pub unsafe fn tick() {
    TIMER.tick();
}

pub unsafe fn init(timer_reload: u16) -> &'static Pit {
    TIMER.init(timer_reload);
    &TIMER
}

pub unsafe fn disable() {
    TIMER.disable();
}

struct Timer {
    when: Instant,
    f: Box<dyn FnOnce() + Send + 'static>,
}

impl fmt::Debug for Timer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Self>())
            .field("when", &self.when)
            .finish()
    }
}

#[derive(Debug)]
pub struct Pit {
    ticks: AtomicU64,
    timer_reload: AtomicU16,
    timers: Mutex<Vec<Timer>>,
}

impl Pit {
    const fn new() -> Self {
        Pit {
            ticks: AtomicU64::new(0),
            timer_reload: AtomicU16::new(0),
            timers: Mutex::new(Vec::new()),
        }
    }

    unsafe fn disable(&self) {
        without_interrupts(|| {
            self.timer_reload.store(0, Ordering::SeqCst);

            let mut cmd = Port::new(MODE_COMMAND_REGISTER);
            let mut set_freq_cmd = PitCommand(0);
            set_freq_cmd.set_channel(Channel::Channel0);
            set_freq_cmd.set_access(Access::LoByteAndHiByte);
            set_freq_cmd.set_operating_mode(OperatingMode::HardwareRetriggerableOneShot);
            set_freq_cmd.set_mode(Mode::Binary);
            cmd.write(set_freq_cmd.0);

            let timer_reload = u16::MAX;
            let lo_byte = (timer_reload & 0xFF) as u8;
            let hi_byte = (timer_reload >> 8) as u8;

            let mut channel0 = Port::new(CHANNEL_0_DATA_PORT);
            channel0.write(lo_byte);
            channel0.write(hi_byte);
        });
    }

    unsafe fn init(&self, timer_reload: u16) {
        assert!(timer_reload > 0);

        if self.timer_reload.compare_and_swap(0, timer_reload, Ordering::SeqCst) != 0 {
            panic!("PIT Already initialised");
        }

        without_interrupts(|| {
            let mut cmd = Port::new(MODE_COMMAND_REGISTER);
            let mut set_freq_cmd = PitCommand(0);
            set_freq_cmd.set_channel(Channel::Channel0);
            set_freq_cmd.set_access(Access::LoByteAndHiByte);
            set_freq_cmd.set_operating_mode(OperatingMode::RateGenerator);
            set_freq_cmd.set_mode(Mode::Binary);
            cmd.write(set_freq_cmd.0);

            let lo_byte = (timer_reload & 0xFF) as u8;
            let hi_byte = (timer_reload >> 8) as u8;

            let mut channel0 = Port::new(CHANNEL_0_DATA_PORT);
            channel0.write(lo_byte);
            channel0.write(hi_byte);
        });
    }

    unsafe fn tick(&self) {
        let reload = self.timer_reload.load(Ordering::SeqCst);
        if reload == 0 {
            // Not initialised
            return;
        }
        self.ticks.fetch_add(1, Ordering::SeqCst);
        let now = self.now();

        let mut timers = self.timers.lock();
        for i in (0..timers.len()).rev() {
            if timers[i].when <= now {
                let t = timers.remove(i);
                (t.f)();
            }
        }
    }
}


#[derive(Debug)]
#[repr(u8)]
enum Channel {
    Channel0 = 0b00,
    Channel1 = 0b01,
    Channel2 = 0b10,
    ReadBack = 0b11,
}

impl From<Channel> for u8 {
    fn from(c: Channel) -> Self {
        c as u8
    }
}

impl From<u8> for Channel {
    fn from(c: u8) -> Self {
        match c {
            0b00 => Channel::Channel0,
            0b01 => Channel::Channel1,
            0b10 => Channel::Channel2,
            0b11 => Channel::ReadBack,
            _ => panic!("Invalid channel: {}", c),
        }
    }
}

#[derive(Debug)]
#[repr(u8)]
enum Access {
    LatchCountValue = 0b00,
    LoByteOnly = 0b01,
    HiByteOnly = 0b10,
    LoByteAndHiByte = 0b11,
}

impl From<Access> for u8 {
    fn from(a: Access) -> Self {
        a as u8
    }
}

impl From<u8> for Access {
    fn from(a: u8) -> Self {
        match a {
            0b00 => Access::LatchCountValue,
            0b01 => Access::LoByteOnly,
            0b10 => Access::HiByteOnly,
            0b11 => Access::LoByteAndHiByte,
            _ => panic!("Unknown access: {}", a),
        }
    }
}

#[derive(Debug)]
#[repr(u8)]
enum OperatingMode {
    InterruptOnTerminalCount = 0b000,
    HardwareRetriggerableOneShot = 0b001,
    RateGenerator = 0b010,
    SquareWaveGenerator = 0b011,
    SoftwareTriggeredStrobe = 0b100,
    HardwareTriggeredStrobe = 0b101,
    RateGenerator2 = 0b110,
    SquareWaveGenerator2 = 0b111,
}

impl From<OperatingMode> for u8 {
    fn from(om: OperatingMode) -> Self {
        om as u8
    }
}

impl From<u8> for OperatingMode {
    fn from(om: u8) -> Self {
        match om {
            0b000 => OperatingMode::InterruptOnTerminalCount,
            0b001 => OperatingMode::HardwareRetriggerableOneShot,
            0b010 => OperatingMode::RateGenerator,
            0b011 => OperatingMode::SquareWaveGenerator,
            0b100 => OperatingMode::SoftwareTriggeredStrobe,
            0b101 => OperatingMode::HardwareTriggeredStrobe,
            0b110 => OperatingMode::RateGenerator2,
            0b111 => OperatingMode::SquareWaveGenerator2,
            _ => panic!("Unknwon operating mode: {}", om),
        }
    }
}

#[derive(Debug)]
#[repr(u8)]
enum Mode {
    Binary = 0,
    Bcd = 1,
}

impl From<u8> for Mode {
    fn from(m: u8) -> Self {
        match m {
            0 => Mode::Binary,
            1 => Mode::Bcd,
            _ => panic!("Invalid mode: {}", m),
        }
    }
}

impl From<Mode> for u8 {
    fn from(m: Mode) -> Self {
        m as u8
    }
}

bitfield::bitfield! {
    struct PitCommand(u8);
    impl Debug;
    pub from into Channel, channel, set_channel: 7, 6;
    pub from into Access, access, set_access: 5, 4;
    pub from into OperatingMode, operating_mode, set_operating_mode: 3, 1;
    pub from into Mode, mode, set_mode: 0, 0;
}

impl InterruptClock for Pit {
    fn now(&self) -> Instant {
        let ticks = self.ticks.load(Ordering::SeqCst);
        let reload = self.timer_reload.load(Ordering::SeqCst) as u64;
        let us = ticks * reload * 1_000_000 / TIMER_FREQ_HZ;
        Instant::from_us(us)
    }

    fn schedule_interrupt(&self, when: Instant, f: Box<dyn FnOnce() + Send + 'static>) {
        without_interrupts(|| {
            let timer = Timer { when, f };
            self.timers.lock().push(timer);
        });
    }
}
