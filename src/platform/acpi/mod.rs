use acpi::{AcpiTables, AcpiHandler, PhysicalMapping, HpetInfo, AcpiError, PlatformInfo, InterruptModel};
use core::ptr::NonNull;
use bootloader::BootInfo;
use crate::platform::hpet::HPETRegisters;
use core::sync::atomic::{AtomicBool, Ordering};
use acpi::platform::Apic;
use crate::PAGE_ALLOCATOR;
use core::ops::RangeInclusive;
//
// pub fn init(offset: usize) -> Result<(), AcpiFooError> {
//     let rsdp = rsdp::find_rsdp(offset).ok_or(AcpiFooError::RsdpNotFound)?;
//     let rsdt = rsdp.rsdt(offset);
//     crate::serial_println!("{:#?}", rsdt.sdts(offset).collect::<alloc::vec::Vec<_>>());
//
//
//     unimplemented!()
// }
//
// pub mod rsdp;
// pub mod rsdt;
// pub mod sdt;
//
// #[derive(Debug)]
// pub enum AcpiFooError {
//     RsdpNotFound,
// }
//
// pub struct AcpiFoo {}
//
// impl AcpiFoo {
//     fn new() -> Self {
//         AcpiFoo {}
//     }
// }

pub struct Acpi {
    pub tables: AcpiTables<AcpiMap>,
}

impl Acpi {
    pub unsafe fn new(offset: usize) -> Self {
        let tables = AcpiTables::search_for_rsdp_bios(AcpiMap { offset })
            .expect("Did not find acpi tables");
        Acpi { tables }
    }

    pub fn find_hpet(&self) -> Result<&'static mut HPETRegisters, AcpiError> {
        static RESOLVE_ONCE: AtomicBool = AtomicBool::new(false);
        if RESOLVE_ONCE.swap(true, Ordering::SeqCst) {
            panic!("Already resolved hpet");
        }

        let hpet_info = HpetInfo::new(&self.tables)?;

        let hpet_register_page = PAGE_ALLOCATOR.get().unwrap().lock().alloc_memory_mapped_registers(hpet_info.base_address as u64).expect("Failed to map hpet registers");
        let hpetregs = hpet_register_page.start_address().as_mut_ptr();
        Ok(unsafe { &mut *hpetregs })
    }

    pub fn platform_info(&self) -> Result<PlatformInfo, AcpiError> {
        PlatformInfo::new(&self.tables)
    }

    pub fn find_apic(&self) -> Option<Apic> {
        match self.platform_info().ok()?.interrupt_model {
            InterruptModel::Apic(acpi) => Some(acpi),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct AcpiMap {
    offset: usize,
}

impl AcpiHandler for AcpiMap {
    unsafe fn map_physical_region<T>(&self, physical_address: usize, size: usize) -> PhysicalMapping<Self, T> {
        let addr = self.offset + physical_address;
        PhysicalMapping {
            physical_start: physical_address,
            virtual_start: NonNull::new(addr as *mut T).unwrap(),
            region_length: size,
            mapped_length: size,
            handler: *self,
        }
    }

    fn unmap_physical_region<T>(&self, region: &PhysicalMapping<Self, T>) {
        // Noop
    }
}
