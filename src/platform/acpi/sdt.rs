use core::marker::PhantomData;
use core::fmt;

pub trait SdtTable {
    unsafe fn from_bytes(b: &'static [u8]) -> Self;
}

#[derive(Debug)]
#[repr(C)]
pub struct SdtHeader {
    signature: [u8; 4],
    length: u32,
    revision: u8,
    checksum: u8,
    oem_id: [u8; 6],
    oem_table_id: [u8; 8],
    oem_revision: u32,
    creator_id: u32,
    creator_revision: u32,
}

#[derive(Debug)]
pub enum SdtError {
    ValidationFailed,
}

#[repr(C)]
pub struct Sdt<T: SdtTable> {
    header: SdtHeader,
    table: PhantomData<T>,
}

impl<T: SdtTable> Sdt<T> {
    pub unsafe fn from_addr(addr: usize, offset: usize) -> Result<&'static Self, SdtError> {
        let ptr = (addr + offset) as *const u8;
        let header = &*(ptr as *const SdtHeader);

        let data = core::slice::from_raw_parts(ptr, header.length as usize);
        let sum = data.iter().copied().fold(0, u8::wrapping_add);
        if sum != 0 {
            return Err(SdtError::ValidationFailed);
        }
        Ok(&*(ptr as *const Self))
    }

    pub fn inner(&self) -> T {
        let ptr = self as *const _ as *const SdtHeader;
        let ptr = unsafe { ptr.offset(1) } as *const u8;
        let data = unsafe { core::slice::from_raw_parts(ptr, self.header.length as usize - core::mem::size_of::<SdtHeader>()) };
        unsafe { T::from_bytes(data) }
    }
}

impl<T: SdtTable> fmt::Debug for Sdt<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Self>())
            .field("header", &self.header)
            .finish()
    }
}
