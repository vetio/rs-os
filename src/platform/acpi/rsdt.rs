use crate::platform::acpi::sdt::{SdtTable, Sdt, SdtHeader};

pub struct Rsdt {
    sdts: &'static [u32],
}

impl SdtTable for Rsdt {
    unsafe fn from_bytes(b: &[u8]) -> Self {
        let ptr = b.as_ptr() as *const u32;
        let len = b.len() / core::mem::size_of::<u32>();
        let sdts = core::slice::from_raw_parts(ptr, len);
        Rsdt {
            sdts,
        }
    }
}

pub struct Xsdt {
    sdts: &'static [u64],
}

impl SdtTable for Xsdt {
    unsafe fn from_bytes(b: &[u8]) -> Self {
        unimplemented!()
    }
}

pub enum RootDescriptor {
    Rsdt(&'static Sdt<Rsdt>),
    Xsdt(&'static Sdt<Xsdt>),
}

impl RootDescriptor {
    pub fn sdts(&self, memory_offset: usize) -> impl Iterator<Item = &'static SdtHeader> {
        match self {
            RootDescriptor::Rsdt(rsdt) => {
                let rsdt =  rsdt.inner();
                rsdt.sdts
                    .iter()
                    .map(move |a| *a as usize + memory_offset)
                    .map(|a| unsafe { &*(a as *const SdtHeader) })
            }
            RootDescriptor::Xsdt(_) => unimplemented!(),
        }
    }
}
