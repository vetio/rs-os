use crate::platform::acpi::rsdt::{Rsdt, RootDescriptor};
use crate::platform::acpi::sdt::Sdt;

#[derive(Debug)]
#[repr(C, packed)]
pub struct RsdpDescriptorV1 {
    signature: [u8; 8],
    checksum: u8,
    oem_id: [u8; 6],
    revision: u8,
    rsdt_address: u32,
}

#[derive(Debug)]
#[repr(C, packed)]
pub struct RsdpDescriptorV2 {
    first: RsdpDescriptorV1,
    length: u32,
    xsdt_address: u64,
    extended_checksum: u8,
    reserved: [u8; 3],
}

#[derive(Debug)]
pub enum Rsdp {
    V1(&'static RsdpDescriptorV1),
    V2(&'static RsdpDescriptorV2),
}

impl Rsdp {
    fn validate(&self) -> bool {
        match self {
            Rsdp::V1(rsdp) => {
                let ptr = (*rsdp) as *const RsdpDescriptorV1 as *const u8;
                let slice = unsafe { core::slice::from_raw_parts(ptr, core::mem::size_of::<RsdpDescriptorV1>()) };
                let mut sum = 0u8;
                for b in slice.iter() {
                    sum = sum.wrapping_add(*b);
                }
                sum == 0
            },
            Rsdp::V2(_) => unimplemented!(),
        }
    }

    pub fn rsdt(&self, memory_offset: usize) -> RootDescriptor {
        match self {
            Rsdp::V1(v1) => {
                let addr = v1.rsdt_address as usize + memory_offset;
                let ptr = addr as *const _;
                RootDescriptor::Rsdt(unsafe { &*ptr })
            }
            Rsdp::V2(_) => unimplemented!()
        }
    }
}

fn test_rsdp(guess: usize) -> Option<&'static RsdpDescriptorV1> {
    let guess_ptr = guess as *const [u8; 8];
    let guess_slice = unsafe { &*guess_ptr };
    if guess_slice == b"RSD PTR " {
        return Some(unsafe { &* (guess_ptr as *const RsdpDescriptorV1) });
    }
    return None;
}

fn check_version(rsdp: &'static RsdpDescriptorV1) -> Option<Rsdp> {
    match rsdp.revision {
        0 => Some(Rsdp::V1(rsdp)),
        1 => Some(Rsdp::V2(unsafe { &*(rsdp as *const _ as *const RsdpDescriptorV2) })),
        _ => None
    }
}

pub fn find_rsdp(offset: usize) -> Option<Rsdp> {
    let ebda_addr = unsafe { ((offset + 0x40E) as *const u16).read_volatile() } as usize;
    let areas = [0xE0000usize..=0xFFFFF, ebda_addr..=(ebda_addr + 1024)];
    areas
        .iter()
        .cloned()
        .flat_map(|a| a.step_by(16))
        .map(|a| a + offset)
        .filter_map(test_rsdp)
        .filter_map(check_version)
        .filter(Rsdp::validate)
        .next()
}
