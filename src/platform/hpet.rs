use crate::mmr::{MemoryMappedRegister, RegisterType};
use volatile::access::{ReadOnly, ReadWrite};
use crate::r#async::timer::{InterruptClock, Instant};
use core::fmt;
use spin::Mutex;
use alloc::collections::VecDeque;
use alloc::boxed::Box;
use x86_64::instructions::interrupts::without_interrupts;
use alloc::vec::Vec;
use crate::platform::acpi::Acpi;
use acpi::AcpiError;
use crate::platform::apic::IOApic;
use crate::r#async::timer::Duration;

static HPET: HPET = unsafe { HPET::new() };

pub fn init(acpi: &Acpi, io_apic: &'static IOApic) -> Result<&'static HPET, HPETTimerError> {
    HPET.init(acpi, io_apic)?;
    Ok(&HPET)
}

pub unsafe fn tick() {
    let mut inner = HPET.inner.lock();
    let inner = inner.as_mut().unwrap();
    let now = inner.now();
    for i in (0..inner.interrupts.len()).rev() {
        if inner.interrupts[i].when <= now {
            let i = inner.interrupts.remove(i);
            (i.f)();
        }
    }
    inner.clear_timers();
}


struct ScheduledInterrupt {
    when: Instant,
    f: Box<dyn FnOnce() + Send + 'static>,
}

impl fmt::Debug for ScheduledInterrupt {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Self>())
            .field("when", &self.when)
            .field("f", &(self.f.as_ref() as *const _ as *const u8 as usize))
            .finish()
    }
}

#[derive(Debug)]
pub enum HPETTimerError {
    CounterNot64Bit,
    Acpi(AcpiError),
}

#[derive(Debug)]
struct HPETInner {
    regs: &'static mut HPETRegisters,
    period: u32,
    timers: &'static mut [HPETTimerRegisters],
    interrupts: Vec<ScheduledInterrupt>,
    io_apic: &'static IOApic,
}

impl HPETInner {
    fn us_to_ticks(&self, us: u64) -> u64 {
        let period = self.period as u64;
        us * (1_000_000_000 / period)
    }

    fn ticks_to_us(&self, ticks: u64) -> u64 {
        let period = self.period as u64;
        ticks * period / 1_000_000_000
    }

    fn clear_timers(&mut self) {
        for t in self.timers.iter_mut() {
            t.configuration.update(|c| c.set_int_enb_cnf(false));
        }
        self.regs.interrupt_status.update(|status| {
            // Writing back the exact same status clears all timer active flags.
            // TODO: Should we clear all at once?
        });
    }

    fn schedule(&mut self, interrupt: ScheduledInterrupt) {
        let tick_deadline = self.us_to_ticks(interrupt.when.as_us());

        self.interrupts.push(interrupt);
        crate::serial_println!("{:#?}", self.regs);

        let io_apic = self.io_apic;
        let timer = self.find_free_or_later_timer().expect("TODO");

        let irq = timer.configuration.read().int_route_cap().trailing_zeros();

        unsafe { io_apic.map_irq(irq, 34) };

        timer.comparator.write(tick_deadline);
        timer.configuration.update(|config| {
            config.set_int_route_cnf(irq as u8);
            config.set_int_enb_cnf(true);
        });

        // assert_eq!(irq as u8, timer.configuration.read().int_route_cnf());
    }

    fn find_free_or_later_timer(&mut self) -> Option<&mut HPETTimerRegisters> {
        // Free
        for t in self.timers.iter_mut() {
            let conf = t.configuration.read();
            if !conf.int_enb_cnf() {
                return Some(t);
            }
        }
        None
    }

    fn now(&self) -> Instant {
        let now = self.regs.main_counter.read();
        Instant::from_us(self.ticks_to_us(now))
    }
}

#[derive(Debug)]
pub struct HPET {
    inner: Mutex<Option<HPETInner>>,
}

impl HPET {
    const unsafe fn new() -> Self {
        HPET {
            inner: Mutex::new(None),
        }
    }

    pub fn init(&self, acpi: &Acpi, io_apic: &'static IOApic) -> Result<(), HPETTimerError> {
        let mut inner = self.inner.lock();

        if inner.is_some() {
            return Ok(());
        }

        let regs = acpi.find_hpet().map_err(HPETTimerError::Acpi)?;

        let caps = regs.capabilities.read();
        if !caps.count_size_cap() {
            return Err(HPETTimerError::CounterNot64Bit);
        }

        let num_timers = caps.num_tim_cap() + 1;
        let timer_ptr = unsafe { (regs as *mut HPETRegisters).offset(1) } as *mut HPETTimerRegisters;
        let timers = unsafe { core::slice::from_raw_parts_mut(timer_ptr, num_timers as usize) };

        let period = caps.counter_clk_period();

        regs.general_configuration.update(|config| {
            config.set_enable_cnf(true);
        });

        for t in timers.iter_mut() {
            t.configuration.update(|config| {
                config.set_int_type_cnf(false);
            })
        }

        *inner = Some(HPETInner { regs, period, timers, interrupts: Vec::new(), io_apic });

        Ok(())
    }
}

impl InterruptClock for &HPET {
    fn now(&self) -> Instant {
        without_interrupts(|| {
            let inner = self.inner.lock();
            let inner = inner.as_ref().unwrap();
            inner.now()
        })
    }

    fn schedule_interrupt(&self, when: Instant, f: Box<dyn FnOnce() + Send + 'static>) {
        without_interrupts(|| {
            let mut inner = self.inner.lock();
            inner.as_mut().unwrap().schedule(ScheduledInterrupt {
                when,
                f,
            });
        });
    }
}

#[repr(C, packed)]
pub struct HPETRegisters {
    capabilities: MemoryMappedRegister<HPETCapabilities, ReadOnly>,
    _reserved1: u64,
    general_configuration: MemoryMappedRegister<HPETGeneralConfiguration, ReadWrite>,
    _reserved2: u64,
    interrupt_status: MemoryMappedRegister<InterruptStatus, ReadWrite>,
    _reserved3: [u8; 0xC8],
    main_counter: MemoryMappedRegister<u64, ReadWrite>,
    _reserved4: u64,
}

impl fmt::Debug for HPETRegisters {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Self>())
            .field("capabilities", &self.capabilities)
            .field("general_configurations", &self.general_configuration)
            .field("interrupt_status", &self.interrupt_status)
            .field("main_counter", &self.main_counter)
            .finish()
    }
}

bitfield::bitfield! {
    #[derive(Clone, Copy)]
    #[repr(transparent)]
    pub struct HPETCapabilities(u64);
    impl Debug;
    u32, counter_clk_period, _: 63, 32;
    u16, vendor_id, _: 31, 16;
    leg_rt_cap, _: 15;
    count_size_cap, _: 13;
    u8, num_tim_cap, _: 12, 8;
    u8, rev_id, _: 7, 0;
}

impl RegisterType for HPETCapabilities {
    type Primitive = u64;

    fn inner(&self) -> &Self::Primitive {
        &self.0
    }

    fn inner_mut(&mut self) -> &mut Self::Primitive {
        &mut self.0
    }
}

impl From<u64> for HPETCapabilities {
    fn from(val: u64) -> Self {
        HPETCapabilities(val)
    }
}

bitfield::bitfield! {
    #[derive(Clone, Copy)]
    #[repr(transparent)]
    pub struct HPETGeneralConfiguration(u64);
    impl Debug;
    leg_rt_cnf, _: 1;
    enable_cnf, set_enable_cnf: 0;
}

impl RegisterType for HPETGeneralConfiguration {
    type Primitive = u64;

    fn inner(&self) -> &Self::Primitive {
        &self.0
    }

    fn inner_mut(&mut self) -> &mut Self::Primitive {
        &mut self.0
    }
}

impl From<u64> for HPETGeneralConfiguration {
    fn from(val: u64) -> Self {
        HPETGeneralConfiguration(val)
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(transparent)]
pub struct InterruptStatus(u64);

impl InterruptStatus {
    pub fn timer_interrupt_active(&self, timer: u8) -> bool {
        assert!(timer < 32);
        let bit = 1 << timer;
        (self.0 & bit) != 0
    }

    pub fn set_timer_interrupt_active(&mut self, timer: u8, active: bool) {
        assert!(timer < 32);
        let bit = 1 << timer;
        if active {
            self.0 |= bit;
        } else {
            self.0 &= !bit;
        }
    }
}

impl RegisterType for InterruptStatus {
    type Primitive = u64;

    fn inner(&self) -> &Self::Primitive {
        &self.0
    }

    fn inner_mut(&mut self) -> &mut Self::Primitive {
        &mut self.0
    }
}

impl From<u64> for InterruptStatus {
    fn from(val: u64) -> Self {
        InterruptStatus(val)
    }
}

#[derive(Debug)]
#[repr(C, packed)]
pub struct HPETTimerRegisters {
    configuration: MemoryMappedRegister<HPETTimerConfigurationRegister, ReadWrite>,
    comparator: MemoryMappedRegister<u64, ReadWrite>,
    interrupt_route: MemoryMappedRegister<HPETInterruptRouteRegister, ReadWrite>,
    _reserved: u64,
}

bitfield::bitfield! {
    #[derive(Clone, Copy)]
    #[repr(transparent)]
    pub struct HPETTimerConfigurationRegister(u64);
    impl Debug;
    u32, int_route_cap, _: 63, 32;
    fsb_int_del_cap, _: 15;
    fsb_en_cap, _: 14;
    u8, int_route_cnf, set_int_route_cnf: 13, 9;
    mode_32, set_mode_32: 8;
    val_set_cnf, _: 6;
    size_cap, _: 5;
    per_int_cap, _: 4;
    type_cnf, _: 3;
    int_enb_cnf, set_int_enb_cnf: 2;
    int_type_cnf, set_int_type_cnf: 1;
}

impl RegisterType for HPETTimerConfigurationRegister {
    type Primitive = u64;

    fn inner(&self) -> &Self::Primitive {
        &self.0
    }

    fn inner_mut(&mut self) -> &mut Self::Primitive {
        &mut self.0
    }
}

impl From<u64> for HPETTimerConfigurationRegister {
    fn from(val: u64) -> Self {
        HPETTimerConfigurationRegister(val)
    }
}

bitfield::bitfield! {
    #[derive(Clone, Copy)]
    #[repr(transparent)]
    pub struct HPETInterruptRouteRegister(u64);
    impl Debug;
}

impl RegisterType for HPETInterruptRouteRegister {
    type Primitive = u64;

    fn inner(&self) -> &Self::Primitive {
        &self.0
    }

    fn inner_mut(&mut self) -> &mut Self::Primitive {
        &mut self.0
    }
}

impl From<u64> for HPETInterruptRouteRegister {
    fn from(val: u64) -> Self {
        HPETInterruptRouteRegister(val)
    }
}

#[cfg(test)]
mod tests {
    use core::mem::size_of;
    use crate::platform::hpet::{HPETRegisters, HPETTimerRegisters, HPETCapabilities, HPETGeneralConfiguration, InterruptStatus};
    use crate::mmr::MemoryMappedRegister;

    fn offset<T, U>(parent: &T, child: &U) -> isize {
        let parent_ptr = parent as *const _ as *const u8;
        let child_ptr = child as *const _ as *const u8;
        unsafe { child_ptr.offset_from(parent_ptr) }
    }

    #[test_case]
    fn hpet_structure_sizes() {
        assert_eq!(0x100, size_of::<HPETRegisters>());
        assert_eq!(0x20, size_of::<HPETTimerRegisters>());
    }

    #[test_case]
    fn hpet_offsets() {
        let test_reg = HPETRegisters {
            capabilities: MemoryMappedRegister::new(HPETCapabilities(0)),
            _reserved1: 0,
            general_configuration: MemoryMappedRegister::new(HPETGeneralConfiguration(0)),
            _reserved2: 0,
            interrupt_status: MemoryMappedRegister::new(InterruptStatus(0)),
            _reserved3: [0; 0xC8],
            main_counter: MemoryMappedRegister::new(0),
            _reserved4: 0,
        };

        assert_eq!(0x000, offset(&test_reg, &test_reg.capabilities));
        assert_eq!(0x010, offset(&test_reg, &test_reg.general_configuration));
        assert_eq!(0x020, offset(&test_reg, &test_reg.interrupt_status));
        assert_eq!(0x0F0, offset(&test_reg, &test_reg.main_counter));
    }
}
