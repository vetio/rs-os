use crate::mmr::{MemoryMappedRegister, RegisterType};
use acpi::platform::{IoApic, Apic};
use volatile::access::{WriteOnly, ReadWrite, ReadOnly};
use core::fmt;
use spin::Mutex;
use alloc::vec::Vec;
use crate::platform::acpi::Acpi;
use x86_64::structures::paging::PhysFrame;
use crate::PAGE_ALLOCATOR;
use tracing::callsite::register;
use core::ops::Add;

static LOCAL_APIC: LocalApic = unsafe { LocalApic::new() };
static IO_APIC: IOApic = unsafe { IOApic::new() };

pub unsafe fn init(acpi: &Acpi) -> (&'static LocalApic, &'static IOApic) {
    let apic = acpi.find_apic().expect("Did not find APIC");
    assert_eq!(1, apic.io_apics.len());

    LOCAL_APIC.init(apic.local_apic_address);
    IO_APIC.init(&apic.io_apics[0]);
    (&LOCAL_APIC, &IO_APIC)
}

pub unsafe fn notify_end_of_interrupt() {
    LOCAL_APIC.notify_end_of_interrupt();
}

struct IOApicInner {
    reg_sel: &'static mut MemoryMappedRegister<u32, WriteOnly>,
    reg_win: &'static mut MemoryMappedRegister<u32, ReadWrite>,
}

impl IOApicInner {
    pub fn apic_id(&mut self) -> IOApicId {
        self.reg_sel.write(0);
        IOApicId(self.reg_win.read())
    }

    pub fn apic_ver(&mut self) -> IOApicVer {
        self.reg_sel.write(1);
        IOApicVer(self.reg_win.read())
    }

    pub fn apic_redirection(&mut self, irq: u32) -> IORedTbl {
        let index = 0x10 + irq * 2;
        self.reg_sel.write(index);
        let lo = self.reg_win.read() as u64;
        self.reg_sel.write(index + 1);
        let hi = self.reg_win.read() as u64;
        IORedTbl((hi << 32) | lo)
    }

    pub fn save_apic_redirection(&mut self, irq: u32, red: IORedTbl) {
        let index = 0x10 + irq * 2;

        let lo = (red.0 & 0xFFFFFFFF) as u32;
        let hi = (red.0 >> 32) as u32;

        self.reg_sel.write(index);
        self.reg_win.write(lo);
        self.reg_sel.write(index + 1);
        self.reg_win.write(hi);
    }
}

pub struct IOApic {
    inner: Mutex<Option<IOApicInner>>,
}

impl fmt::Debug for IOApic {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut inner = self.inner.lock();
        let inner = inner.as_mut().unwrap();
        let id = inner.apic_id();
        let ver = inner.apic_ver();
        let mut redirections = Vec::with_capacity(ver.max_redirection_entry() as usize);
        for irq in 0..ver.max_redirection_entry() {
            redirections.push(inner.apic_redirection(irq));
        }
        f.debug_struct("IOApic")
            .field("ioapicid", &id)
            .field("ioapicver", &ver)
            .field("redirections", &redirections)
            .finish()
    }
}

impl IOApic {
    const unsafe fn new() -> Self {
        IOApic { inner: Mutex::new(None) }
    }

    unsafe fn init(&self, apic: &IoApic) -> Result<(), ApicError> {
        let mut inner = self.inner.lock();
        if inner.is_some() {
            return Err(ApicError::AlreadyInitialised);
        }

        let register_page = PAGE_ALLOCATOR.get().unwrap().lock().alloc_memory_mapped_registers(apic.address as u64)
            .expect("Failed to map IOAPIC registers");

        let addr = register_page.start_address();
        let ptr_sel = addr.as_mut_ptr();
        let ptr_data = (addr + 0x10u64).as_mut_ptr();
        let (reg_sel, reg_win) = unsafe { (&mut *ptr_sel, &mut *ptr_data) };

        *inner = Some(IOApicInner { reg_sel, reg_win });

        Ok(())
    }

    pub unsafe fn map_irq(&self, irq: u32, vector: u8) {
        let mut inner = self.inner.lock();
        let inner = inner.as_mut().unwrap();

        let mut redir = inner.apic_redirection(irq);
        redir.set_vector(vector as u64);
        redir.set_mask(false);
        redir.set_pin_polarity(false);
        redir.set_trigger_mode(false);
        inner.save_apic_redirection(irq, redir);
    }
}

#[derive(Debug)]
enum ApicError {
    NoApicFound,
    AlreadyInitialised,
}

#[repr(C, align(16))]
#[derive(Debug, Copy, Clone)]
struct LocalApicRegister(u32);

impl RegisterType for LocalApicRegister {
    type Primitive = u32;

    fn inner(&self) -> &Self::Primitive {
        &self.0
    }

    fn inner_mut(&mut self) -> &mut Self::Primitive {
        &mut self.0
    }
}

impl From<u32> for LocalApicRegister {
    fn from(val: u32) -> Self {
        LocalApicRegister(val)
    }
}

#[repr(C, align(16))]
struct LocalApicRegisters {
    _reserved0: [LocalApicRegister; 2],
    lapic_id: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    lapic_version: MemoryMappedRegister<LocalApicRegister, ReadOnly>,
    _resetved1: [LocalApicRegister; 4],
    task_priority: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    arbitration_priority: MemoryMappedRegister<LocalApicRegister, ReadOnly>,
    processor_priority: MemoryMappedRegister<LocalApicRegister, ReadOnly>,
    eoi: MemoryMappedRegister<LocalApicRegister, WriteOnly>,
    remote_read: MemoryMappedRegister<LocalApicRegister, ReadOnly>,
    logical_destination: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    destination_format: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    spurious_interrupt_vector: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    in_service: [MemoryMappedRegister<LocalApicRegister, ReadOnly>; 8],
    trigger_mode: [MemoryMappedRegister<LocalApicRegister, ReadOnly>; 8],
    interrupt_request: [MemoryMappedRegister<LocalApicRegister, ReadOnly>; 8],
    error_status: MemoryMappedRegister<LocalApicRegister, ReadOnly>,
    _reserved2: [LocalApicRegister; 6],
    lvt_corrected_machine_check_interrupt: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    interrupt_command: [MemoryMappedRegister<LocalApicRegister, ReadWrite>; 2],
    lvt_timer: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    lvt_thermal_sensor: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    lvt_performance_monitoring_counters: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    lvt_lint0: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    lvt_lint1: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    lvt_error: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    initial_count: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    current_count: MemoryMappedRegister<LocalApicRegister, ReadOnly>,
    _reserved3: [LocalApicRegister; 4],
    divide_configuration: MemoryMappedRegister<LocalApicRegister, ReadWrite>,
    _reservec4: LocalApicRegister,
}

impl fmt::Debug for LocalApicRegisters {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(core::any::type_name::<Self>())
            .field("lapic_id", &self.lapic_id)
            .field("lapic_version", &self.lapic_version)
            .field("task_priority", &self.task_priority)
            .field("arbitration_priority", &self.arbitration_priority)
            .field("processor_priority", &self.processor_priority)
            .field("remote_read", &self.remote_read)
            .field("logical_destination", &self.logical_destination)
            .field("destination_format", &self.destination_format)
            .field("spurious_interrupt_vector", &self.spurious_interrupt_vector)
            .field("in_service", &self.in_service)
            .field("trigger_mode", &self.trigger_mode)
            .field("interrupt_request", &self.interrupt_request)
            .field("error_status", &self.error_status)
            .field("lvt_corrected_machine_check_interrupt", &self.lvt_corrected_machine_check_interrupt)
            .field("interrupt_command", &self.interrupt_command)
            .field("lvt_timer", &self.lvt_timer)
            .field("lvt_thermal_sensor", &self.lvt_thermal_sensor)
            .field("lvt_performance_monitoring_counters", &self.lvt_performance_monitoring_counters)
            .field("lvt_lint0", &self.lvt_lint0)
            .field("lvt_lint1", &self.lvt_lint1)
            .field("lvt_error", &self.lvt_error)
            .field("initial_count", &self.initial_count)
            .field("current_count", &self.current_count)
            .field("divide_configuration", &self.divide_configuration)
            .field("_reservec4", &self._reservec4)
            .finish()
    }
}

#[derive(Debug)]
pub struct LocalApic {
    registers: Mutex<Option<&'static mut LocalApicRegisters>>,
}

impl LocalApic {
    const unsafe fn new() -> Self {
        LocalApic { registers: Mutex::new(None) }
    }

    unsafe fn init(&self, base_address: u64) -> Result<(), ApicError> {
        let mut registers = self.registers.lock();

        if registers.is_some() {
            return Err(ApicError::AlreadyInitialised);
        }

        let register_page = PAGE_ALLOCATOR.get().unwrap().lock().alloc_memory_mapped_registers(base_address).expect("Failed to map local apic registers");
        *registers = Some(&mut *(register_page.start_address().as_mut_ptr()));

        registers.as_mut().unwrap().spurious_interrupt_vector.update(|siv| {
            siv.0 |= 0x100;
        });

        Ok(())
    }

    pub unsafe fn notify_end_of_interrupt(&self) {
        let mut registers = self.registers.lock();
        let registers = registers.as_mut().unwrap();
        registers.eoi.write(LocalApicRegister(0));
    }
}

bitfield::bitfield! {
    pub struct IOApicId(u32);
    impl Debug;
    id, _: 27, 24;
}

bitfield::bitfield! {
    pub struct IOApicVer(u32);
    impl Debug;
    version, _: 8, 0;
    max_redirection_entry, _: 23, 16;
}

bitfield::bitfield! {
    pub struct IORedTbl(u64);
    impl Debug;
    pub vector, set_vector: 7, 0;
    pub delivery_mode, set_delivery_mode: 10, 8;
    pub destination_mode, set_destination_mode: 11;
    pub delivery_status, set_delivery_status: 12;
    pub pin_polarity, set_pin_polarity: 13;
    pub trigger_mode, set_trigger_mode: 15;
    pub mask, set_mask: 16;
    pub destination, set_destination: 63, 56;
}

#[cfg(test)]
mod tests {
    use crate::platform::apic::LocalApicRegisters;

    #[test_case]
    fn struct_sizes() {
        assert_eq!(0x400, core::mem::size_of::<LocalApicRegisters>());
    }
}
