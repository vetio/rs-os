use core::marker::PhantomData;
use volatile::Volatile;
use volatile::access::{Readable, Writable, ReadOnly, ReadWrite, WriteOnly};
use core::fmt;

pub trait RegisterType: Sized {
    type Primitive: Copy + Into<Self>;
    fn inner(&self) -> &Self::Primitive;
    fn inner_mut(&mut self) -> &mut Self::Primitive;
}

impl RegisterType for u32 {
    type Primitive = Self;

    fn inner(&self) -> &Self::Primitive {
        self
    }

    fn inner_mut(&mut self) -> &mut Self::Primitive {
        self
    }
}
impl RegisterType for u64 {
    type Primitive = Self;

    fn inner(&self) -> &Self::Primitive {
        self
    }

    fn inner_mut(&mut self) -> &mut Self::Primitive {
        self
    }
}

#[repr(transparent)]
pub struct MemoryMappedRegister<T: RegisterType, A> {
    val: T,
    _access: PhantomData<A>,
}

impl<T: RegisterType + fmt::Debug, A: Readable> fmt::Debug for MemoryMappedRegister<T, A> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.val.fmt(f)
    }
}

impl<T: RegisterType, A> MemoryMappedRegister<T, A> {
    pub fn new(val: T) -> Self {
        MemoryMappedRegister {
            val,
            _access: PhantomData,
        }
    }
}

impl<T: RegisterType, A: Readable> MemoryMappedRegister<T, A> {
    pub fn read(&self) -> T {
        let reg = Volatile::new_read_only(self.val.inner());
        reg.read().into()
    }
}

impl<T: RegisterType, A: Readable + Writable> MemoryMappedRegister<T, A> {
    pub fn update<F: FnOnce(&mut T)>(&mut self, f: F) {
        let mut reg = Volatile::new(self.val.inner_mut());
        reg.update(
            |p| {
                let mut val: T = (*p).into();
                f(&mut val);
                *p = *val.inner();
            }
        );
    }
}

impl<T: RegisterType, A: Writable> MemoryMappedRegister<T, A> {
    pub fn write(&mut self, val: T) {
        let mut reg = Volatile::new_write_only(self.val.inner_mut());
        reg.write(*val.inner())
    }
}
