use crate::collections::ring_buffer::RingBuffer;
use alloc::format;
use alloc::string::String;
use alloc::vec::Vec;
use core::fmt::Write;
use spin::Mutex;
use tracing::span::{Attributes, Record};
use tracing::{Event, Id, Instrument, Metadata, Subscriber};
use x86_64::instructions::interrupts::without_interrupts;

pub static LOGGER: SerialLogger = SerialLogger::new();

struct SpanData {
    text: String,
    parent: Option<Id>,
}

impl SpanData {
    fn println(&self) {
        crate::serial_println!("{}", self.text);
    }
}

pub struct SerialLogger {
    spans: Mutex<Vec<SpanData>>,
}

impl SerialLogger {
    pub const fn new() -> Self {
        SerialLogger {
            spans: Mutex::new(Vec::new()),
        }
    }

    fn print_outer_span(&self, id: &Id) -> u32 {
        let spans = self.spans.lock();
        let span = &spans[id.into_u64() as usize - 1];
        let indent = match span.parent.as_ref() {
            Some(parent_id) => self.print_outer_span(parent_id) + 4,
            None => 0,
        };
        for _ in 0..indent {
            crate::serial_print!(" ");
        }
        span.println();

        indent
    }
}

impl Subscriber for SerialLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        true
    }

    fn new_span(&self, span: &Attributes<'_>) -> Id {
        let metadata = span.metadata();
        let text = format!(
            "{:?} {}: {:?}",
            metadata.level(),
            metadata.target(),
            span.values()
        );
        let parent = span.parent().cloned();

        let span = SpanData { text, parent };

        let mut spans = self.spans.lock();
        spans.push(span);
        let id = spans.len();
        drop(spans);
        Id::from_u64(id as u64)
    }

    fn record(&self, span: &Id, values: &Record) {
        self.print_outer_span(span);
        crate::serial_println!("{:?}", values);
    }

    fn record_follows_from(&self, span: &Id, follows: &Id) {}

    fn event(&self, event: &Event<'_>) {
        crate::serial_println!(
            "{:?} {}: {:?}",
            event.metadata().level(),
            event.metadata().target(),
            event
        );
    }

    fn enter(&self, span: &Id) {
        crate::serial_print!("enter span: ");
        self.print_outer_span(span);
    }

    fn exit(&self, span: &Id) {
        crate::serial_print!("exit span: ");
        self.print_outer_span(span);
    }
}

// #[derive(Debug)]
// pub struct MemoryLogger {
//     buffer: Mutex<RingBuffer<2048>>,
// }
//
// impl MemoryLogger {
//     pub const fn new() -> Self {
//         MemoryLogger {
//             buffer: Mutex::new(RingBuffer::<2048>::new()),
//         }
//     }
//
//     pub fn content_len(&self) -> usize {
//         without_interrupts(|| self.buffer.lock().len())
//     }
//
//     pub fn copy_end_to_slice(&self, target: &mut [u8]) {
//         without_interrupts(|| {
//             self.buffer.lock().copy_end_to_slice(target);
//         });
//     }
//
//     pub fn clear(&self) {
//         without_interrupts(|| {
//             self.buffer.lock().clear();
//         });
//     }
// }

// impl Log for MemoryLogger {
//     fn enabled(&self, _metadata: &Metadata) -> bool {
//         true
//     }
//
//     fn log(&self, record: &Record) {
//         without_interrupts(|| {
//             let mut buffer = self.buffer.lock();
//             write!(
//                 *buffer,
//                 "{} {} - {}\r\n",
//                 record.level(),
//                 record.target(),
//                 record.args()
//             )
//             .expect("log failed");
//         });
//     }
//
//     fn flush(&self) {}
// }

#[cfg(test)]
pub mod test {
    use crate::logging::SerialLogger;

    // use crate::logging::MemoryLogger;
    //
    // pub static TEST_LOGGER: MemoryLogger = MemoryLogger::new();
    //
    // #[test_case]
    // fn test_memory_logger_error() {
    //     TEST_LOGGER.clear();
    //     log::error!("Test error");
    //
    //     let logged_len = TEST_LOGGER.content_len();
    //     let mut logged_text = [0u8; 4096];
    //     if logged_len >= 4096 {
    //         panic!("Excessive message logged");
    //     }
    //     TEST_LOGGER.copy_end_to_slice(&mut logged_text[..logged_len]);
    //     let logged_text = core::str::from_utf8(&logged_text[..logged_len])
    //         .expect("Failed to read logged text as str");
    //
    //     assert!(logged_text.contains(concat!("ERROR ", module_path!(), " - Test error")));
    // }
    //
    // #[test_case]
    // fn test_memory_logger_warn() {
    //     TEST_LOGGER.clear();
    //     log::warn!("Test error");
    //
    //     let logged_len = TEST_LOGGER.content_len();
    //     let mut logged_text = [0u8; 4096];
    //     if logged_len >= 4096 {
    //         panic!("Excessive message logged");
    //     }
    //     TEST_LOGGER.copy_end_to_slice(&mut logged_text[..logged_len]);
    //     let logged_text = core::str::from_utf8(&logged_text[..logged_len])
    //         .expect("Failed to read logged text as str");
    //
    //     assert!(logged_text.contains(concat!("WARN ", module_path!(), " - Test error")));
    // }
    //
    // #[test_case]
    // fn test_memory_logger_info() {
    //     TEST_LOGGER.clear();
    //     log::info!("Test error");
    //
    //     let logged_len = TEST_LOGGER.content_len();
    //     let mut logged_text = [0u8; 4096];
    //     if logged_len >= 4096 {
    //         panic!("Excessive message logged");
    //     }
    //     TEST_LOGGER.copy_end_to_slice(&mut logged_text[..logged_len]);
    //     let logged_text = core::str::from_utf8(&logged_text[..logged_len])
    //         .expect("Failed to read logged text as str");
    //
    //     assert!(logged_text.contains(concat!("INFO ", module_path!(), " - Test error")));
    // }
    //
    // #[test_case]
    // fn test_memory_logger_debug() {
    //     TEST_LOGGER.clear();
    //     log::debug!("Test error");
    //
    //     let logged_len = TEST_LOGGER.content_len();
    //     let mut logged_text = [0u8; 4096];
    //     if logged_len >= 4096 {
    //         panic!("Excessive message logged");
    //     }
    //     TEST_LOGGER.copy_end_to_slice(&mut logged_text[..logged_len]);
    //     let logged_text = core::str::from_utf8(&logged_text[..logged_len])
    //         .expect("Failed to read logged text as str");
    //
    //     assert!(logged_text.contains(concat!("DEBUG ", module_path!(), " - Test error")));
    // }
    //
    // #[test_case]
    // fn test_memory_logger_trace() {
    //     TEST_LOGGER.clear();
    //     log::trace!("Test error");
    //
    //     let logged_len = TEST_LOGGER.content_len();
    //     let mut logged_text = [0u8; 4096];
    //     if logged_len >= 4096 {
    //         panic!("Excessive message logged");
    //     }
    //     TEST_LOGGER.copy_end_to_slice(&mut logged_text[..logged_len]);
    //     let logged_text = core::str::from_utf8(&logged_text[..logged_len])
    //         .expect("Failed to read logged text as str");
    //
    //     assert!(logged_text.contains(concat!("TRACE ", module_path!(), " - Test error")));
    // }
}
