#![no_std]
#![no_main]
#![feature(abi_x86_interrupt)]
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;
use lazy_static::lazy_static;
use rs_os::gdt::DOUBLE_FAULT_IST_INDEX;
use rs_os::qemu::{exit_qemu, QemuExitCode};
use volatile::Volatile;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};

#[no_mangle]
pub extern "C" fn _start() -> ! {
    rs_os::gdt::init();
    init_test_idt();

    rs_os::serial_print!("stack_overflow...");

    stack_overflow();

    panic!("Execution continued after stack overflow");
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    rs_os::panic::test_panic(info)
}

static X: u32 = 0;
#[allow(unconditional_recursion)]
fn stack_overflow() {
    stack_overflow();
    Volatile::new(&X).read();
}

lazy_static! {
    static ref TEST_IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        unsafe {
            idt.double_fault
                .set_handler_fn(double_fault_handler)
                .set_stack_index(DOUBLE_FAULT_IST_INDEX);
        }
        idt
    };
}

fn init_test_idt() {
    TEST_IDT.load();
}

extern "x86-interrupt" fn double_fault_handler(
    _stack_frame: &mut InterruptStackFrame,
    _error_code: u64,
) -> ! {
    rs_os::serial_println!("[Ok]");
    exit_qemu(QemuExitCode::Success)
}
