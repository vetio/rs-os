#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(rs_os::test_runner::test_runner)]
#![reexport_test_harness_main = "test_main"]

rs_os::test_start!();

#[test_case]
fn test_println() {
    rs_os::println!("test_println");
}
