#![no_std]
#![no_main]

use core::panic::PanicInfo;
use rs_os::qemu;
use rs_os::qemu::{exit_qemu, QemuExitCode};

#[no_mangle]
pub extern "C" fn _start() -> ! {
    rs_os::serial_print!("should_panic...");
    should_panic();
    rs_os::serial_println!("[Test did not panic]");
    exit_qemu(QemuExitCode::Failed)
}

#[panic_handler]
fn panic(_: &PanicInfo) -> ! {
    rs_os::serial_println!("[Ok]");
    qemu::exit_qemu(QemuExitCode::Success)
}

fn should_panic() {
    assert_eq!(1, 2);
}
