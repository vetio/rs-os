#![no_std]
#![no_main]
#![feature(asm)]

extern crate alloc;

use alloc::boxed::Box;
use alloc::rc::Rc;
use alloc::vec;
use alloc::vec::Vec;
use rs_os::context::Context;
use rs_os::panic::catch_unwind;
use rs_os::prelude::*;
use rs_os::unwind::Processed;
use volatile::Volatile;
use rs_os::CONTEXT;
use acpi::HpetInfo;
use rs_os::platform::hpet::{HPETRegisters, HPET};
use rs_os::r#async::timer::InterruptClock;
use rs_os::r#async::timer::Duration;
use rs_os::platform::apic::{IOApic, LocalApic};
use rs_os::interrupts::{PIC_1_OFFSET};
use rs_os::vga::print;
use rs_os::r#async::timer::Delay;

rs_os::start!();

struct TestDrop {
    foo: u8,
}

impl TestDrop {
    fn new() -> Self {
        TestDrop { foo: 0 }
    }
}

impl Drop for TestDrop {
    fn drop(&mut self) {
        Volatile::new(&mut self.foo).write(42);
    }
}

fn main() {
    println!("Hello World");

    let mut executor = rs_os::r#async::executor::Executor::new();
    executor.spawn(rs_os::r#async::task::Task::new(async {
        println!("Waiting...");
        Delay::new(Duration::from_s(10)).await;
        println!("Done");
    }));
    executor.run();

    println!("It did not crash");
}
